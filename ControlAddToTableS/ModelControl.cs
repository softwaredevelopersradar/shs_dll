﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Runtime.CompilerServices;
using System.Windows.Controls.WpfPropertyGrid;
using SHS_DLL;

namespace ControlAddToTableS
{
    public enum InterferenceP : byte
    {
        [Description("Без модуляции")]
        FirstParam = 0,
        [Description("КФМ")]
        SecondParam = 1,
        [Description("ЛЧМ")]
        ThirdParam = 4,
        [Description("ЛЧМ2")]
        FourthParam = 5
    }

    [CategoryOrder(CategoryAdd, 1)]
    [RefreshProperties(RefreshProperties.All)]
    public class ModelControl : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        #region Public
        public const string CategoryAdd = "Добавить";
        public static bool brows = true;
        public string descr = "Манипуляция, мкс";
        public string displayName = "Манипуляция, мкс";
        #endregion

        #region Private
        private int fKHz;
        private int dev;
        private int man;
        private int spsc;
        private int dFKHz;
        private byte power;
        private InterferenceP interferenceParam;
        private Antenna antenna;
        #endregion

        [DataMember]
        [Category(CategoryAdd)]
        [Description("F, кГц")]
        [DisplayName("F, кГц")]
        [PropertyOrder(1)]
        [Browsable(true)]
        [Required]
        public int FKHz
        {
            get => fKHz;
            set
            {
                if (fKHz == value) return;
                fKHz = value;
                //OnPropertyChanged();
            }
        }

        [DataMember]
        [Category(CategoryAdd)]
        [PropertyOrder(2)]
        [Description("ΔF, кГц")]
        [DisplayName("ΔF, кГц")]
        [Browsable(true)]
        [Required]
        public int DFKHz
        {
            get => dFKHz;
            set
            {
                if (dFKHz == value) return;
                dFKHz = value;
                //OnPropertyChanged();
            }
        }

        [DataMember]
        [Category(CategoryAdd)]
        [PropertyOrder(3)]
        [Description("Параметры помехи")]
        [DisplayName("Параметры помехи")]
        [NotifyParentProperty(true)]
        [RefreshProperties(RefreshProperties.All)]
        [Browsable(true)]
        [Required]
        public InterferenceP InterferenceParam
        {
            get => interferenceParam;
            set
            {
                if (interferenceParam == value) return;
                interferenceParam = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [Category(CategoryAdd)]
        [PropertyOrder(4)]
        [NotifyParentProperty(true)]
        [RefreshProperties(RefreshProperties.All)]
        [Description("Девиация, кГц")]
        [DisplayName("Девиация, кГц")]
        [Browsable(false)]
        [Required]
        public int Dev
        {
            get => dev;
            set
            {
                if (dev == value) return;
                dev = value;
                //OnPropertyChanged();
            }
        }

        [DataMember]
        [Category(CategoryAdd)]
        [PropertyOrder(5)]
        [NotifyParentProperty(true)]
        [Description("Манипуляция, мкс")]
        [DisplayName("Манипуляция, мкс")]
        [Browsable(false)]
        [Required]
        public int Man
        {
            get => man;
            set
            {
                if (man == value) return;
                man = value;
                //OnPropertyChanged();
            }
        }

        [DataMember]
        [Category(CategoryAdd)]
        [PropertyOrder(5)]
        [NotifyParentProperty(true)]
        [Description("Скорость сканирования, кГц/c")]
        [DisplayName("Скорость сканирования, кГц/c")]
        [Browsable(false)]
        [Required]
        public int SpSc
        {
            get => spsc;
            set
            {
                if (spsc == value) return;
                spsc = value;
                //OnPropertyChanged();
            }
        }

        [DataMember]
        [Category(CategoryAdd)]
        [PropertyOrder(6)]
        [NotifyParentProperty(true)]
        [Description("Мощность (0-255)")]
        [DisplayName("Мощность (0-255)")]
        [Browsable(true)]
        [Required]
        public byte Power
        {
            get => power;
            set
            {
                if (power == value) return;
                power = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [Category(CategoryAdd)]
        [PropertyOrder(7)]
        [NotifyParentProperty(true)]
        [Description("Антенна")]
        [DisplayName("Антенна")]
        [Browsable(true)]
        [Required]
        public Antenna Antenna
        {
            get => antenna;
            set
            {
                if (antenna == value) return;
                antenna = value;
            }
        }

        #region Methods

        public ModelControl Clone()
        {
            return new ModelControl
            {
                FKHz = fKHz,
                DFKHz = dFKHz,
                InterferenceParam = interferenceParam,
                Dev = dev,
                Man = man,
                Power = power
            };
        }

        public void Update(ModelControl newSettings)
        {
            FKHz = newSettings.FKHz;
            DFKHz = newSettings.DFKHz;
            InterferenceParam = newSettings.InterferenceParam;
            Dev = newSettings.Dev;
            Man = newSettings.Man;
            Power = newSettings.Power;
        }

        public bool Compare(ModelControl classSettings)
        {
            if (classSettings.FKHz != FKHz ||
                classSettings.DFKHz != DFKHz || classSettings.InterferenceParam != InterferenceParam || classSettings.Dev != Dev || classSettings.Man != Man || classSettings.Power != Power)
                return false;
            return true;
        }

        #endregion
    }
}
