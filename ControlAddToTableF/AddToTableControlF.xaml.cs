﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace ControlAddToTableF
{
    /// <summary>
    /// Логика взаимодействия для AddToTableControlF.xaml
    /// </summary>
    public partial class AddToTableControlF : UserControl
    {
        public AddToTableControlF()
        {
            InitializeComponent();
            Init();
        }

        public event EventHandler<ModelControl> OnButton;
        public ModelControl model = new ModelControl();

        private void Init()
        {
            model = PropertyMap.SelectedObject as ModelControl;
        }

        public void OnChange(ModelControl Model)
        {
            PropertyMap.SelectedObject = new ModelControl() { Register = Model.Register, FStart = Model.FStart, FStop = Model.FStop, KStart = Model.KStart, KStop = Model.KStop };
            Init();
        }

        private void ButApply_Click(object sender, RoutedEventArgs e)
        {
            ModelControl control = new ModelControl();
            control = new ModelControl() { Register = model.Register, FStart = model.FStart, FStop = model.FStop, KStart = model.KStart, KStop = model.KStop };

            OnButton?.Invoke(this, control);
        }
    }
}
