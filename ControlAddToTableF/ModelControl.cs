﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Windows.Controls.WpfPropertyGrid;

namespace ControlAddToTableF
{
    [CategoryOrder(CategoryAdd, 1)]
    [RefreshProperties(RefreshProperties.All)]
    public class ModelControl : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        #region Public
        public const string CategoryAdd = "Добавить";
        #endregion

        #region Private
        private byte register;
        private int fStart;
        private int fStop;
        private byte kStart;
        private byte kStop;
        #endregion

        [DataMember]
        [Category(CategoryAdd)]
        [Description("Id")]
        [DisplayName("Id")]
        [PropertyOrder(1)]
        [Browsable(true)]
        [Required]
        public byte Register
        {
            get => register;
            set
            {
                if (register == value) return;
                register = value;
                //OnPropertyChanged();
            }
        }

        [DataMember]
        [Category(CategoryAdd)]
        [Description("Fstart, МГц")]
        [DisplayName("Fstart, МГц")]
        [PropertyOrder(2)]
        [Browsable(true)]
        [Required]
        public int FStart
        {
            get => fStart;
            set
            {
                if (fStart == value) return;
                fStart = value;
                //OnPropertyChanged();
            }
        }

        [DataMember]
        [Category(CategoryAdd)]
        [PropertyOrder(3)]
        [Description("Fstop, MГц")]
        [DisplayName("Fstop, MГц")]
        [Browsable(true)]
        [Required]
        public int FStop
        {
            get => fStop;
            set
            {
                if (fStop == value) return;
                fStop = value;
                //OnPropertyChanged();
            }
        }

        [DataMember]
        [Category(CategoryAdd)]
        [PropertyOrder(4)]
        [Description("Kstart")]
        [DisplayName("Kstart")]
        [NotifyParentProperty(true)]
        [RefreshProperties(RefreshProperties.All)]
        [Browsable(true)]
        [Required]
        public byte KStart
        {
            get => kStart;
            set
            {
                if (kStart == value) return;
                kStart = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [Category(CategoryAdd)]
        [PropertyOrder(5)]
        [NotifyParentProperty(true)]
        [RefreshProperties(RefreshProperties.All)]
        [Description("Kstop")]
        [DisplayName("Kstop")]
        [Browsable(true)]
        [Required]
        public byte KStop
        {
            get => kStop;
            set
            {
                if (kStop == value) return;
                kStop = value;
                //OnPropertyChanged();
            }
        }

        #region Methods

        public ModelControl Clone()
        {
            return new ModelControl
            {
                Register = register,
                FStart = fStart,
                FStop = fStop,
                KStart = kStart,
                KStop = kStop
            };
        }

        public void Update(ModelControl newSettings)
        {
            Register = newSettings.register;
            FStart = newSettings.fStart;
            FStop = newSettings.fStop;
            KStart = newSettings.kStart;
            KStop = newSettings.kStop;
        }

        public bool Compare(ModelControl classSettings)
        {
            if (classSettings.Register != Register ||
                classSettings.FStart != FStart || classSettings.FStop != FStop || classSettings.KStart != KStart || classSettings.KStop != KStop)
                return false;
            return true;
        }

        #endregion
    }
}
