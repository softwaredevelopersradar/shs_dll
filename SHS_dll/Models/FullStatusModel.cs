﻿using SHS_DLL.Models;

namespace SHS_DLL
{
    public class FullStatusModel
    {
        public byte CodeError { get; set; }
        public bool[] Amplifiers { get; set; }
        public bool Relay { get; set; }
        public LetterState[] ParamAmp { get; set; }
        public AmpCodes Amp { get; set; }

        public FullStatusModel() { }

        public FullStatusModel(byte CodeError) { this.CodeError = CodeError; }

        public FullStatusModel(byte CodeError, bool[] Amplifiers, bool Relay, LetterState[] ParamAmp, AmpCodes Amp)
        {
            this.CodeError = CodeError;
            this.Amplifiers = Amplifiers;
            this.Relay = Relay;
            this.ParamAmp = ParamAmp;
            this.Amp = Amp;
        }
    }

}
