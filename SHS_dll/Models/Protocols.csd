namespace SHS_DLL.Models
{
	extern enum SHS_DLL.Antenna
	extern enum SHS_DLL.AmpCodes
	extern enum SHS_DLL.FPSCodes
	extern enum SHS_DLL.Spoofing

	struct GpsGlonass
	{
		bool GpsL1
        bool GpsL2
        bool GlnssL1
        bool GlnssL2
	}

	struct BeidouGalileo
	{
		bool BeidouL1
        bool BeidouL2
        bool GalileoL1
        bool GalileoL2
	}

	struct MessageHeader
	{
		byte SenderAddress
		byte ReceiverAddress
		SHS_DLL.AmpCodes Code
		byte PatternCounter
		byte InformationLength
	}

	struct DefaultMessage
	{
		MessageHeader Header
		byte ErrorCode
	}

	struct LetterStateRequest
	{
		MessageHeader Header
		byte NumberOfLetter
	}

	struct LetterState
	{
		byte Synthesizer
		byte Radiation
		byte Temperature
		byte Current
		byte AmplifierPower
		byte Error
	}

	struct LetterStateResponse
	{
		MessageHeader Header
		byte ErrorCode
		LetterState[(Header.InformationLength - 1) / LetterState.BinarySize] LetterStateParam
	}

	struct LetterStateIncludingAntenna
	{
		byte Synthesizer
		byte Radiation
		byte Temperature
		byte Current
		byte AmplifierPower
		byte Error
		SHS_DLL.Antenna AntennaParam
	}

	struct LetterStateIncludingAntennaResponse
	{
		MessageHeader Header
		byte ErrorCode
		LetterStateIncludingAntenna[(Header.InformationLength - 1) / LetterStateIncludingAntenna.BinarySize] LetterStateIncludingAntennaParam
	}

	struct FWSParametersWithPower
	{
		int24 Freq
		byte Modulation
		byte Deviation
		byte Manipulation
		byte Duration
		byte PowerLevel
	}

	struct FWSParameters
	{
		int24 Freq
		byte Modulation
		byte Deviation
		byte Manipulation
		byte Duration
	}

	struct FWSParametersWithAntenna
	{
		int24 Freq
		byte Modulation
		byte Deviation
		byte Manipulation
		byte Duration
		SHS_DLL.Antenna AntennaParam
	}

	struct FWSParametersWithAntennaPower
	{
		int24 Freq
		byte Modulation
		byte Deviation
		byte Manipulation
		byte Duration
		SHS_DLL.Antenna AntennaParam
		byte PowerLevel
	}

	struct TurnOnRadiationIRIFRSRequest
	{
		MessageHeader Header
		int24 Duration
		FWSParameters[(Header.InformationLength - 3) / FWSParameters.BinarySize] FWSParam
	}

	struct TurnOnRadiationIRIFRSWithPowerRequest
	{
		MessageHeader Header
		int24 Duration
		FWSParametersWithPower[(Header.InformationLength - 3) / FWSParametersWithPower.BinarySize] FWSWithPowerParam
	}

	struct TurnOnRadiationIRIFRSWithAntennaPowerRequest
	{
		MessageHeader Header
		int24 Duration
		FWSParametersWithAntennaPower[(Header.InformationLength - 3) / FWSParametersWithPower.BinarySize] FWSWithPowerParam
	}

	struct TurnOnRadiationIRIFRSWithAntennaRequest
	{
		MessageHeader Header
		int24 Duration
		FWSParametersWithAntenna[(Header.InformationLength - 3) / FWSParametersWithPower.BinarySize] FWSParam
	}

	struct EnableSpoofing
	{
		MessageHeader Header
	}

	struct EnableSpoofingWithPower
	{
		MessageHeader Header
		byte PowerLevel
	}

	struct NavigationGpsGlonnasRequest
	{
		MessageHeader Header
		GpsGlonass GpsGlonassParam
	}

	struct NavigationGpsGlonnasWihtPowerRequest
	{
		MessageHeader Header
		GpsGlonass GpsGlonassParam
		byte PowerLevel
	}

	struct NavigationGpsGlonnasBeidouGalileoRequest
	{
		MessageHeader Header
		GpsGlonass GpsGlonassParam
		BeidouGalileo BeidouGalileoParam
	}

	struct NavigationGpsGlonnasBeidouGalileoWihtPowerRequest
	{
		MessageHeader Header
		GpsGlonass GpsGlonassParam
		BeidouGalileo BeidouGalileoParam
		byte PowerLevel
	}

	struct NavigationGpsGlonnasBeidouGalileoSecondRequest
	{
		MessageHeader Header
		GpsGlonass GpsGlonassParam
		BeidouGalileo BeidouGalileoParam
		SHS_DLL.Antenna AntennaParam
		SHS_DLL.Spoofing SpoofingState
	}

	struct NavigationGpsGlonnasBeidouGalileoSecondWihtPowerRequest
	{
		MessageHeader Header
		GpsGlonass GpsGlonassParam
		BeidouGalileo BeidouGalileoParam
		SHS_DLL.Antenna AntennaParam
		SHS_DLL.Spoofing SpoofingState
		byte PowerLevel
	}

	struct PreselectorSettings
	{
		MessageHeader Header
		byte TurningOnAmplifiersLowByte
		byte TurningOnAmplifiersHighByte
	}

	struct TurnOnRadiationIRIFRSNavigationRequest
	{
		MessageHeader Header
		int24 Duration
		byte NavigationSystem
		byte NavigationPower
		FWSParameters[(Header.InformationLength - 6) / FWSParameters.BinarySize] FWSParam
	}

	struct TurnOnRadiationIRIFRSNavigationWithPowerRequest
	{
		MessageHeader Header
		int24 Duration
		byte NavigationSystem
		byte NavigationPower
		FWSParametersWithPower[(Header.InformationLength - 5) / FWSParametersWithPower.BinarySize] FWSWithPowerParam
	}

	struct FullStatusResponse
	{
		MessageHeader Header
		byte ErrorCode
		byte TurningOnAmplifiersLowByte
		byte TurningOnAmplifiersHighByte
		byte Rele
		LetterState[(Header.InformationLength - 4) / LetterState.BinarySize] LetterStateParam
	}

	struct ShortMessageHeader
	{
		byte SenderReceiverAddress
		SHS_DLL.FPSCodes Code
		byte InformationLength
	}

	struct AdditionalMessageHeader
	{
		0x04
		0x05
		0x47
		byte PatternCounter
		byte InformationLength
	}

	struct FPSDefaultMessageResponse
	{
		ShortMessageHeader Header
		byte ErrorCode
		byte CRC
	}

	struct FPSRadiatOFFShortRequest
	{
		ShortMessageHeader Header
		byte Channel
		byte CRC
	}

	struct FPSRadiatOFFRequest
	{
		AdditionalMessageHeader AdditionalHeader
		ShortMessageHeader Header
		byte Channel
		byte CRC
	}

	struct FPSParameters
	{
		byte Id
		int24 FreqL
		int24 FreqH
		byte KL
		byte KH
	}

	struct AmplifierParameters
	{
		byte Id
		int24 Fn
		int24 Fv
		byte Kn
		byte Kv
		byte Status
	}

	struct ChannelAmplifierParameterShortRequest
	{
		ShortMessageHeader Header
		FPSParameters[(Header.InformationLength) / FPSParameters.BinarySize] FPSParams
		byte CRC
	}

	struct ChannelAmplifierParameterRequest
	{
		AdditionalMessageHeader AdditionalHeader
		ShortMessageHeader Header
		FPSParameters[(Header.InformationLength) / FPSParameters.BinarySize] FPSParams
		byte CRC
	}

	struct FPSDefaultShortRequest
	{
		ShortMessageHeader Header
		byte CRC
	}

	struct FPSDefaultRequest
	{
		AdditionalMessageHeader AdditionalHeader
		ShortMessageHeader Header
		byte CRC
	}

	struct FPSDeviceStatusResponse
	{
		ShortMessageHeader Header
		byte FirstLetter
		byte SecondLetter
		byte CRC
	}

	struct FPSPositionSwitchesShortRequest
	{
		ShortMessageHeader Header
		byte NumberOfSwitches
		byte SwitchPosition
		byte CRC
	}

	struct FPSPositionSwitchesRequest
	{
		AdditionalMessageHeader AdditionalHeader
		ShortMessageHeader Header
		byte NumberOfSwitches
		byte SwitchPosition
		byte CRC
	}

	struct FPSParamNaViRadOnShortRequest
	{
		ShortMessageHeader Header
		byte SupressedNaVi
		byte NavigationPower
		byte CRC
	}

	struct FPSParamNaViRadOnRequest
	{
		AdditionalMessageHeader AdditionalHeader
		ShortMessageHeader Header
		byte SupressedNaVi
		byte NavigationPower
		byte CRC
	}

	struct FPSParamsIRIRadOnShortRequest
	{
		ShortMessageHeader Header
		byte NumberOfIRI
		FWSParameters[(Header.InformationLength - 1) / FWSParameters.BinarySize] FWSParams
		byte CRC
	}

	struct FPSParamsIRIRadOnRequest
	{
		AdditionalMessageHeader AdditionalHeader
		ShortMessageHeader Header
		byte NumberOfIRI
		FWSParameters[(Header.InformationLength - 1) / FWSParameters.BinarySize] FWSParams
		byte CRC
	}

	struct FPSParametersShortRequest
	{
		ShortMessageHeader Header
		byte[Header.InformationLength] Indexes
		byte CRC
	}

	struct FPSParametersRequest
	{
		AdditionalMessageHeader AdditionalHeader
		ShortMessageHeader Header
		byte[Header.InformationLength] Indexes
		byte CRC
	}

	struct FPSParametersResponse
	{
		ShortMessageHeader Header
		AmplifierParameters[Header.InformationLength / AmplifierParameters.BinarySize] AmplifierParam
		byte CRC
	}
}