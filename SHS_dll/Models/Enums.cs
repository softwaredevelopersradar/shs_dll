﻿using System.ComponentModel;

namespace SHS_DLL
{
    #region Enum
    public enum Antenna : byte
    {
        [Description("Log")]
        Log = 0,
        [Description("Omni")]
        Omni = 1
    }

    public enum Spoofing : byte
    {
        Off,
        On
    }

    public enum FPSCodes
    {
        STATUS = 0x04,
        RADIAT_OFF = 0x24,
        PARAM_IRI_RADIAT_ON = 0x28,
        TEST_GNSS = 0x30,
        PARAM_NAVI_RADIAT_ON = 0x34,
        SET_PARAM_AMPL = 0x40,
        GET_PARAM_AMPL = 0x41,
        SAVE_PARAM_AMPL = 0x42,
        GET_PO_VERSION = 0x43,
        SWITCH_POSITION = 0x31
    }

    public enum AmpCodes
    {
        RELAY_SWITCHING = 22,
        RADIAT_OFF_APP = 13,
        STATUS_APP,
        PARAM_FWS_APP,
        RESET_APP,
        GNSS_APP,
        NAVI_RADIAT_ON,
        SPOOF_APP = 19,
        NAVI_RADIAT_ON_TWO_CHANNEL,
        PARAM_PRESELECTOR = 23,
        PARAM_FWS_GNSS,
        RADIAT_OFF_PRESELECTOR_ON,
        FULL_STATUS,
        SET_RECEIVING_SWITCHES,
        SET_TRANSMISSION_SWITCHES,
        ANTENNA_STATE = 30,
        PARAM_FWS_APP_ANTENNA = 50,
        NAVI_RADIAT_ON_ANTENNA,
        ANTENNA_STATE_SECOND 
    }
    #endregion
}
