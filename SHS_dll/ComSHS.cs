﻿using SHS_DLL.Models;
using System;
using System.Collections;
using System.Linq;
using System.Threading.Tasks;

namespace SHS_DLL
{
    public class ComSHS : BaseCom
    {
        #region Private
        public byte _fpsAddressReceiver = 0x02;
        private byte _addressSender;
        private byte _addressReceiver;
        private byte _counter;
        #endregion

        #region Constr
        public ComSHS(byte AddressAWP, byte AddressSHS)
        {
            _addressSender = AddressAWP;
            _addressReceiver = AddressSHS;
        }
        #endregion

        #region SendCommand
        /// <summary>
        /// Выключить излучение 
        /// </summary>
        public async Task<byte> SendFPSRadiatOFF(byte channel)
        {
            var header = new AdditionalMessageHeader(CheckCounter(), ShortMessageHeader.BinarySize + 2);
            var shortHeader = new ShortMessageHeader(GetFPSAddrReceiverSender(), FPSCodes.RADIAT_OFF, 1);
            var message = FPSRadiatOFFRequest.ToBinary(header, shortHeader, channel, 0);
            message[message.Length - 1] = CalculateCRC(message, 5);
            FPSDefaultMessageResponse result = await SendReceiveAsync<FPSDefaultMessageResponse>(message, FPSDefaultMessageResponse.BinarySize);
            return result.ErrorCode;
        }

        public async Task<byte> SendFPSRadiatOFFShort(byte channel)
        {
            var shortHeader = new ShortMessageHeader(GetFPSAddrReceiverSender(), FPSCodes.RADIAT_OFF, 1);
            var message = FPSRadiatOFFShortRequest.ToBinary(shortHeader, channel, 0);
            message[message.Length - 1] = CalculateCRC(message, 0);
            FPSDefaultMessageResponse result = await SendReceiveAsync<FPSDefaultMessageResponse>(message, FPSDefaultMessageResponse.BinarySize);
            return result.ErrorCode;
        }

        /// <summary>
        /// Установить параметр усилителя в канале
        /// </summary>
        public async Task<byte> FPSSendParam(FPSParameters[] fParam)
        {
            try
            {
                if (fParam == null)
                {
                    return 1;
                }

                var header = new AdditionalMessageHeader(CheckCounter(), (byte)(ShortMessageHeader.BinarySize + fParam.Length * FPSParameters.BinarySize + 1));
                var shortHeader = new ShortMessageHeader(GetFPSAddrReceiverSender(), FPSCodes.SET_PARAM_AMPL, (byte)(fParam.Length * FPSParameters.BinarySize));
                var message = ChannelAmplifierParameterRequest.ToBinary(header, shortHeader, fParam, 0);
                message[message.Length - 1] = CalculateCRC(message, 5);

                FPSDefaultMessageResponse result = await SendReceiveAsync<FPSDefaultMessageResponse>(message, FPSDefaultMessageResponse.BinarySize);
                return result.ErrorCode;
            }
            catch
            {
                return 1;
            }
        }

        public async Task<byte> FPSSendParamShort(FPSParameters[] fParam)
        {
            try
            {
                if (fParam == null)
                {
                    return 1;
                }

                var shortHeader = new ShortMessageHeader(GetFPSAddrReceiverSender(), FPSCodes.SET_PARAM_AMPL, (byte)(fParam.Length * FPSParameters.BinarySize));
                var message = ChannelAmplifierParameterShortRequest.ToBinary(shortHeader, fParam, 0);
                message[message.Length - 1] = CalculateCRC(message, 0);

                FPSDefaultMessageResponse result = await SendReceiveAsync<FPSDefaultMessageResponse>(message, FPSDefaultMessageResponse.BinarySize);
                return result.ErrorCode;
            }
            catch
            {
                return 1;
            }
        }

        /// <summary>
        /// Запросить Статус устройства
        /// </summary>
        public async Task<byte> FPSSendDeviceStatus()
        {
            var header = new AdditionalMessageHeader(CheckCounter(), (byte)(ShortMessageHeader.BinarySize + 1));
            var shortHeader = new ShortMessageHeader(GetFPSAddrReceiverSender(), FPSCodes.STATUS, 0);
            var message = FPSDefaultRequest.ToBinary(header, shortHeader, 0);
            message[message.Length - 1] = CalculateCRC(message, 5);

            FPSDefaultMessageResponse result = await SendReceiveAsync<FPSDefaultMessageResponse>(message, FPSDefaultMessageResponse.BinarySize);
            return result.ErrorCode;
        }

        public async Task<byte> FPSSendDeviceStatusShort()
        {
            var shortHeader = new ShortMessageHeader(GetFPSAddrReceiverSender(), FPSCodes.STATUS, 0);
            var message = FPSDefaultShortRequest.ToBinary(shortHeader, 0);
            message[message.Length - 1] = CalculateCRC(message, 0);

            FPSDefaultMessageResponse result = await SendReceiveAsync<FPSDefaultMessageResponse>(message, FPSDefaultMessageResponse.BinarySize);
            return result.ErrorCode;
        }

        /// <summary>
        /// Установить тестовый сигнал для GNSS без модуляции
        /// </summary>
        public async Task<byte> FPSSendSetTestGNSS()
        {
            var header = new AdditionalMessageHeader(CheckCounter(), (byte)(ShortMessageHeader.BinarySize + 1));
            var shortHeader = new ShortMessageHeader(GetFPSAddrReceiverSender(), FPSCodes.TEST_GNSS, 0);
            var message = FPSDefaultRequest.ToBinary(header, shortHeader, 0);
            message[message.Length - 1] = CalculateCRC(message, 5);

            FPSDefaultMessageResponse result = await SendReceiveAsync<FPSDefaultMessageResponse>(message, FPSDefaultMessageResponse.BinarySize);
            return result.ErrorCode;
        }

        public async Task<byte> FPSSendSetTestGNSSShort()
        {
            var shortHeader = new ShortMessageHeader(GetFPSAddrReceiverSender(), FPSCodes.TEST_GNSS, 0);
            var message = FPSDefaultShortRequest.ToBinary(shortHeader, 0);
            message[message.Length - 1] = CalculateCRC(message, 0);

            FPSDefaultMessageResponse result = await SendReceiveAsync<FPSDefaultMessageResponse>(message, FPSDefaultMessageResponse.BinarySize);
            return result.ErrorCode;
        }

        /// <summary>
        /// Запросить версию ПО
        /// </summary>
        public async Task<byte> FPSSendGetVersion()
        {
            var header = new AdditionalMessageHeader(CheckCounter(), (byte)(ShortMessageHeader.BinarySize + 1));
            var shortHeader = new ShortMessageHeader(GetFPSAddrReceiverSender(), FPSCodes.GET_PO_VERSION, 0);
            var message = FPSDefaultRequest.ToBinary(header, shortHeader, 0);
            message[message.Length - 1] = CalculateCRC(message, 5);

            FPSDefaultMessageResponse result = await SendReceiveAsync<FPSDefaultMessageResponse>(message, FPSDefaultMessageResponse.BinarySize);
            return result.ErrorCode;
        }

        public async Task<byte> FPSSendGetVersionShort()
        {
            var shortHeader = new ShortMessageHeader(GetFPSAddrReceiverSender(), FPSCodes.GET_PO_VERSION, 0);
            var message = FPSDefaultShortRequest.ToBinary(shortHeader, 0);
            message[message.Length - 1] = CalculateCRC(message, 0);

            FPSDefaultMessageResponse result = await SendReceiveAsync<FPSDefaultMessageResponse>(message, FPSDefaultMessageResponse.BinarySize);
            return result.ErrorCode;
        }

        /// <summary>
        /// Сохранить в параметры усилителя в канале
        /// </summary>
        public async Task<byte> FPSSendSaveParam()
        {
            var header = new AdditionalMessageHeader(CheckCounter(), (byte)(ShortMessageHeader.BinarySize + 1));
            var shortHeader = new ShortMessageHeader(GetFPSAddrReceiverSender(), FPSCodes.SAVE_PARAM_AMPL, 0);
            var message = FPSDefaultRequest.ToBinary(header, shortHeader, 0);
            message[message.Length - 1] = CalculateCRC(message, 5);

            FPSDefaultMessageResponse result = await SendReceiveAsync<FPSDefaultMessageResponse>(message, FPSDefaultMessageResponse.BinarySize);
            return result.ErrorCode;
        }

        public async Task<byte> FPSSendSaveParamShort()
        {
            var shortHeader = new ShortMessageHeader(GetFPSAddrReceiverSender(), FPSCodes.SAVE_PARAM_AMPL, 0);
            var message = FPSDefaultShortRequest.ToBinary(shortHeader, 0);
            message[message.Length - 1] = CalculateCRC(message, 0);

            FPSDefaultMessageResponse result = await SendReceiveAsync<FPSDefaultMessageResponse>(message, FPSDefaultMessageResponse.BinarySize);
            return result.ErrorCode;
        }

        /// <summary>
        /// Установить положение коммутаторов 
        /// </summary>
        public async Task<byte> FPSSendPositionSwitches(byte numberSwitches, byte switchPosition)
        {
            var header = new AdditionalMessageHeader(CheckCounter(), (byte)(ShortMessageHeader.BinarySize + 3));
            var shortHeader = new ShortMessageHeader(GetFPSAddrReceiverSender(), FPSCodes.SWITCH_POSITION, 2);
            var message = FPSPositionSwitchesRequest.ToBinary(header, shortHeader, numberSwitches, switchPosition, 0);
            message[message.Length - 1] = CalculateCRC(message, 5);

            FPSDefaultMessageResponse result = await SendReceiveAsync<FPSDefaultMessageResponse>(message, FPSDefaultMessageResponse.BinarySize);
            return result.ErrorCode;
        }

        public async Task<byte> FPSSendPositionSwitchesShort(byte numberSwitches, byte switchPosition)
        {
            var shortHeader = new ShortMessageHeader(GetFPSAddrReceiverSender(), FPSCodes.SWITCH_POSITION, 2);
            var message = FPSPositionSwitchesShortRequest.ToBinary(shortHeader, numberSwitches, switchPosition, 0);
            message[message.Length - 1] = CalculateCRC(message, 0);

            FPSDefaultMessageResponse result = await SendReceiveAsync<FPSDefaultMessageResponse>(message, FPSDefaultMessageResponse.BinarySize);
            return result.ErrorCode;
        }

        /// <summary>
        /// Установить параметры помехи для навигации и включить излучение 
        /// </summary>
        public async Task<byte> FPSSendSetParamNaViRadOn(byte supressedNaVi, byte navigationPower)
        {
            var header = new AdditionalMessageHeader(CheckCounter(), (byte)(ShortMessageHeader.BinarySize + 3));
            var shortHeader = new ShortMessageHeader(GetFPSAddrReceiverSender(), FPSCodes.PARAM_NAVI_RADIAT_ON, 2);
            var message = FPSParamNaViRadOnRequest.ToBinary(header, shortHeader, supressedNaVi, navigationPower, 0);
            message[message.Length - 1] = CalculateCRC(message, 5);

            FPSDefaultMessageResponse result = await SendReceiveAsync<FPSDefaultMessageResponse>(message, FPSDefaultMessageResponse.BinarySize);
            return result.ErrorCode;
        }

        public async Task<byte> FPSSendSetParamNaViRadOnShort(byte supressedNaVi, byte navigationPower)
        {
            var shortHeader = new ShortMessageHeader(GetFPSAddrReceiverSender(), FPSCodes.PARAM_NAVI_RADIAT_ON, 2);
            var message = FPSParamNaViRadOnShortRequest.ToBinary(shortHeader, supressedNaVi, navigationPower, 0);
            message[message.Length - 1] = CalculateCRC(message, 0);

            FPSDefaultMessageResponse result = await SendReceiveAsync<FPSDefaultMessageResponse>(message, FPSDefaultMessageResponse.BinarySize);
            return result.ErrorCode;
        }

        /// <summary>
        /// Установить параметры помехи ИРИ и включить излучение
        /// </summary>
        public async Task<byte> FPSSendSetParamsIRI(FWSParameters[] tParamFWS)
        {
            if (tParamFWS == null)
            {
                return 1;
            }

            for (int i = 0; i < tParamFWS.Length; i++)
            {
                tParamFWS[i].Deviation = CreateDev(tParamFWS[i].Modulation, tParamFWS[i].Deviation);
            }

            var header = new AdditionalMessageHeader(CheckCounter(), (byte)(ShortMessageHeader.BinarySize + tParamFWS.Length * FWSParameters.BinarySize + 2));
            var shortHeader = new ShortMessageHeader(GetFPSAddrReceiverSender(), FPSCodes.PARAM_IRI_RADIAT_ON, (byte)(tParamFWS.Length * FWSParameters.BinarySize + 1));
            var message = FPSParamsIRIRadOnRequest.ToBinary(header, shortHeader, (byte)tParamFWS.Length, tParamFWS, 0);
            message[message.Length - 1] = CalculateCRC(message, 5);

            FPSDefaultMessageResponse result = await SendReceiveAsync<FPSDefaultMessageResponse>(message, FPSDefaultMessageResponse.BinarySize);
            return result.ErrorCode;
        }

        public async Task<byte> FPSSendSetParamsIRIShort(FWSParameters[] tParamFWS)
        {
            if (tParamFWS == null)
            {
                return 1;
            }

            for (int i = 0; i < tParamFWS.Length; i++)
            {
                tParamFWS[i].Deviation = CreateDev(tParamFWS[i].Modulation, tParamFWS[i].Deviation);
            }

            var shortHeader = new ShortMessageHeader(GetFPSAddrReceiverSender(), FPSCodes.PARAM_IRI_RADIAT_ON, (byte)(tParamFWS.Length * FWSParameters.BinarySize + 1));
            var message = FPSParamsIRIRadOnShortRequest.ToBinary(shortHeader, (byte)tParamFWS.Length, tParamFWS, 0);
            message[message.Length - 1] = CalculateCRC(message, 0);

            FPSDefaultMessageResponse result = await SendReceiveAsync<FPSDefaultMessageResponse>(message, FPSDefaultMessageResponse.BinarySize);
            return result.ErrorCode;
        }

        /// <summary>
        /// Запросить параметр усилителя в канале
        /// </summary>
        public async Task<AmplifierParameters[]> FPSSendGetParam(byte[] indexes)
        {
            var header = new AdditionalMessageHeader(CheckCounter(), (byte)(ShortMessageHeader.BinarySize + (byte)indexes.Length + 1));
            var shortHeader = new ShortMessageHeader(GetFPSAddrReceiverSender(), FPSCodes.GET_PARAM_AMPL, (byte)indexes.Length);
            var message = FPSParametersRequest.ToBinary(header, shortHeader, indexes, 0);
            message[message.Length - 1] = CalculateCRC(message, 5);

            FPSParametersResponse result = await SendReceiveAsync<FPSParametersResponse>(message, ShortMessageHeader.BinarySize + AmplifierParameters.BinarySize * indexes.Length + 1);
            return result.AmplifierParam;
        }

        public async Task<AmplifierParameters[]> FPSSendGetParamShort(byte[] indexes)
        {
            var shortHeader = new ShortMessageHeader(GetFPSAddrReceiverSender(), FPSCodes.GET_PARAM_AMPL, (byte)indexes.Length);
            var message = FPSParametersShortRequest.ToBinary(shortHeader, indexes, 0);
            message[message.Length - 1] = CalculateCRC(message, 0);

            FPSParametersResponse result = await SendReceiveAsync<FPSParametersResponse>(message, ShortMessageHeader.BinarySize + AmplifierParameters.BinarySize * indexes.Length + 1);
            return result.AmplifierParam;
        }

        /// <summary>
        /// Состояние литеры
        /// </summary>
        public async Task<LetterState[]> SendStatus(byte letter, bool isSixLetter = false)
        {
            var header = new MessageHeader(this._addressSender, this._addressReceiver, AmpCodes.STATUS_APP, CheckCounter(), 1);
            var message = LetterStateRequest.ToBinary(header, letter);
            LetterStateResponse letterStateResponse = await SendReceiveAsync<LetterStateResponse>(message, isSixLetter ? DefaultMessage.BinarySize + 35 : DefaultMessage.BinarySize + 30);

            return letterStateResponse.LetterStateParam;
        }

        /// <summary>
        /// Состояние литеры (с учетом антенны)
        /// </summary>
        /// <param name="letter">Литера, 0 - все литеры</param>
        /// <returns></returns>
        public async Task<LetterStateIncludingAntenna[]> SendStatusAntennaState(byte letter, bool isSixLetter = false)
        {
            var header = new MessageHeader(this._addressSender, this._addressReceiver, AmpCodes.ANTENNA_STATE, CheckCounter(), 1);
            var message = LetterStateRequest.ToBinary(header, letter);
            LetterStateIncludingAntennaResponse letterStateResponse = await SendReceiveAsync<LetterStateIncludingAntennaResponse>(message, isSixLetter ? DefaultMessage.BinarySize + 42 : DefaultMessage.BinarySize + 35);

            return letterStateResponse.LetterStateIncludingAntennaParam;
        }

        /// <summary>
        /// Состояние литеры (с учетом антенны). Для 2-го скворечника Грозы-С 
        /// </summary>
        /// <param name="letter">Литера, 0 - все литеры</param>
        /// <returns></returns>
        public async Task<LetterStateIncludingAntenna[]> SendStatusAntennaStateSecond(byte letter)
        {
            var header = new MessageHeader(this._addressSender, this._addressReceiver, AmpCodes.ANTENNA_STATE_SECOND, CheckCounter(), 1);
            var message = LetterStateRequest.ToBinary(header, letter);
            LetterStateIncludingAntennaResponse letterStateResponse = await SendReceiveAsync<LetterStateIncludingAntennaResponse>(message, DefaultMessage.BinarySize + 35);

            return letterStateResponse.LetterStateIncludingAntennaParam;
        }

        /// <summary>
        /// Настройка приёмных коммутаторов
        /// </summary>
        /// <param name="sectorsNumber">Содержит два байта, возможные значения для каждого 1..6 – номер сектора; 7 – ненапрвленная антенна( 2,4 )</param>
        /// <returns>Результат</returns>
        public async Task<PreselectorSettings> SendSetReceivingSwitches(byte[] sectorsNumber)
        {
            var header = new MessageHeader(this._addressSender, this._addressReceiver, AmpCodes.SET_RECEIVING_SWITCHES, CheckCounter(), 2);
            var message = PreselectorSettings.ToBinary(header, sectorsNumber[0], sectorsNumber[1]);
            PreselectorSettings defaultMessage = await SendReceiveAsync<PreselectorSettings>(message, PreselectorSettings.BinarySize);
            return defaultMessage;
        }

        /// <summary>
        /// Настройка передающих коммутаторов
        /// </summary>
        /// <param name="sectorsNumber">Содержит два байта, возможные значения для каждого 1..6 – номер сектора; 7 – ненапрвленная антенна( 2,4 )</param>
        /// <returns>Результат</returns>
        public async Task<PreselectorSettings> SendSetTransmissionSwitches(byte[] sectorsNumber)
        {
            var header = new MessageHeader(this._addressSender, this._addressReceiver, AmpCodes.SET_TRANSMISSION_SWITCHES, CheckCounter(), 2);
            var message = PreselectorSettings.ToBinary(header, sectorsNumber[0], sectorsNumber[1]);
            PreselectorSettings defaultMessage = await SendReceiveAsync<PreselectorSettings>(message, PreselectorSettings.BinarySize);
            return defaultMessage;
        }

        /// <summary>
        /// Выключить излучение
        /// </summary>
        public async Task<DefaultMessage> SendRadiatOff(byte letter)
        {
            var header = new MessageHeader(this._addressSender, this._addressReceiver, AmpCodes.RADIAT_OFF_APP, CheckCounter(), 1);
            var message = DefaultMessage.ToBinary(header, letter);
            DefaultMessage defaultMessage = await SendReceiveAsync<DefaultMessage>(message, DefaultMessage.BinarySize);
            return defaultMessage;
        }


        /// <summary>
        /// Отправляет запрос статуса на формирователь и ресетит усилители
        /// </summary>
        public async Task<DefaultMessage> SendReset(byte letter)
        {
            var header = new MessageHeader(this._addressSender, this._addressReceiver, AmpCodes.RESET_APP, CheckCounter(), 1);
            var message = DefaultMessage.ToBinary(header, letter);
            DefaultMessage defaultMessage = await SendReceiveAsync<DefaultMessage>(message, DefaultMessage.BinarySize);
            return defaultMessage;
        }

        /// <summary>
        /// Установить параметры РП для ИРИ ФРЧ и включить излучение заданной длительности
        /// </summary>
        public async Task<DefaultMessage> SendSetParamFWS(int iDuration, FWSParameters[] tParamFWS)
        {
            if (tParamFWS == null || tParamFWS.Length == 0)
            {
                return new DefaultMessage();
            }

            for (int i = 0; i < tParamFWS.Length; i++)
            {
                tParamFWS[i].Deviation = CreateDev(tParamFWS[i].Modulation, tParamFWS[i].Deviation);
            }

            var header = new MessageHeader(this._addressSender, this._addressReceiver, AmpCodes.PARAM_FWS_APP, CheckCounter(), (byte)(FWSParameters.BinarySize * tParamFWS.Length + 3));
            
            var message = TurnOnRadiationIRIFRSRequest.ToBinary(header, iDuration, tParamFWS);

            DefaultMessage defaultMessage = await SendReceiveAsync<DefaultMessage>(message, DefaultMessage.BinarySize);
            return defaultMessage;
        }

        /// <summary>
        /// Установить параметры РП для ИРИ ФРЧ и включить излучение заданной длительности c мощностью
        /// </summary>
        public async Task<DefaultMessage> SendSetParamFWS(int iDuration, FWSParametersWithPower[] tParamFWS)
        {
            if (tParamFWS == null || tParamFWS.Length == 0)
            {
                return new DefaultMessage();
            }

            for (int i = 0; i < tParamFWS.Length; i++)
            {
                tParamFWS[i].Deviation = CreateDev(tParamFWS[i].Modulation, tParamFWS[i].Deviation);
            }

            var header = new MessageHeader(this._addressSender, this._addressReceiver, AmpCodes.PARAM_FWS_APP, CheckCounter(), (byte)(FWSParametersWithPower.BinarySize * tParamFWS.Length + 3));

            var message = TurnOnRadiationIRIFRSWithPowerRequest.ToBinary(header, iDuration, tParamFWS);

            DefaultMessage defaultMessage = await SendReceiveAsync<DefaultMessage>(message, DefaultMessage.BinarySize);
            return defaultMessage;
        }


        /// <summary>
        /// Установить параметры РП для ИРИ ФРЧ и включить излучение заданной длительности (Для 2-го скворечника Грозы-С)
        /// </summary>
        public async Task<DefaultMessage> SendSetParamFWSWithAntenna(int iDuration, FWSParametersWithAntenna[] tParamFWS)
        {
            if (tParamFWS == null || tParamFWS.Length == 0)
            {
                return new DefaultMessage();
            }

            for (int i = 0; i < tParamFWS.Length; i++)
            {
                tParamFWS[i].Deviation = CreateDev(tParamFWS[i].Modulation, tParamFWS[i].Deviation);
            }

            var header = new MessageHeader(this._addressSender, this._addressReceiver, AmpCodes.PARAM_FWS_APP_ANTENNA, CheckCounter(), (byte)(FWSParametersWithAntenna.BinarySize * tParamFWS.Length + 3));

            var message = TurnOnRadiationIRIFRSWithAntennaRequest.ToBinary(header, iDuration, tParamFWS);

            DefaultMessage defaultMessage = await SendReceiveAsync<DefaultMessage>(message, DefaultMessage.BinarySize);
            return defaultMessage;
        }

        /// <summary>
        /// Установить параметры РП для ИРИ ФРЧ и включить излучение заданной длительности c мощностью (Для 2-го скворечника Грозы-С)
        /// </summary>
        public async Task<DefaultMessage> SendSetParamFWSWithAntenna(int iDuration, FWSParametersWithAntennaPower[] tParamFWS)
        {
            if (tParamFWS == null || tParamFWS.Length == 0)
            {
                return new DefaultMessage();
            }

            for (int i = 0; i < tParamFWS.Length; i++)
            {
                tParamFWS[i].Deviation = CreateDev(tParamFWS[i].Modulation, tParamFWS[i].Deviation);
            }

            var header = new MessageHeader(this._addressSender, this._addressReceiver, AmpCodes.PARAM_FWS_APP_ANTENNA, CheckCounter(), (byte)(FWSParametersWithAntennaPower.BinarySize * tParamFWS.Length + 3));

            var message = TurnOnRadiationIRIFRSWithAntennaPowerRequest.ToBinary(header, iDuration, tParamFWS);

            DefaultMessage defaultMessage = await SendReceiveAsync<DefaultMessage>(message, DefaultMessage.BinarySize);
            return defaultMessage;
        }


        /// <summary>
        /// Установить подавляемые системы навигации и включить излучение
        /// </summary>
        public async Task<DefaultMessage> SendSetGNSS(GpsGlonass gnss)
        {
            var header = new MessageHeader(this._addressSender, this._addressReceiver, AmpCodes.GNSS_APP, CheckCounter(), GpsGlonass.BinarySize);
            var message = NavigationGpsGlonnasRequest.ToBinary(header, gnss);
            DefaultMessage defaultMessage = await SendReceiveAsync<DefaultMessage>(message, DefaultMessage.BinarySize);
            return defaultMessage;
        }

        /// <summary>
        /// Установить подавляемые системы навигации и включить излучение 2
        /// </summary>
        public async Task<DefaultMessage> SendSetGNSS(GpsGlonass gpsGlonass, BeidouGalileo beidouGalileo)
        {
            var header = new MessageHeader(this._addressSender, this._addressReceiver, AmpCodes.NAVI_RADIAT_ON, CheckCounter(), GpsGlonass.BinarySize + BeidouGalileo.BinarySize);
            var message = NavigationGpsGlonnasBeidouGalileoRequest.ToBinary(header, gpsGlonass, beidouGalileo);
            DefaultMessage defaultMessage = await SendReceiveAsync<DefaultMessage>(message, DefaultMessage.BinarySize);
            return defaultMessage;
        }

        /// <summary>
        /// Установить подавляемые системы навигации и включить излучение с мощностью
        /// </summary>
        public async Task<DefaultMessage> SendSetGNSS(GpsGlonass gnss, byte Power)
        {
            var header = new MessageHeader(this._addressSender, this._addressReceiver, AmpCodes.GNSS_APP, CheckCounter(), GpsGlonass.BinarySize + 1);
            var message = NavigationGpsGlonnasWihtPowerRequest.ToBinary(header, gnss, Power);
            DefaultMessage defaultMessage = await SendReceiveAsync<DefaultMessage>(message, DefaultMessage.BinarySize);
            return defaultMessage;
        }

        /// <summary>
        /// Установить подавляемые системы навигации и включить излучение 2 с мощностью
        /// </summary>
        public async Task<DefaultMessage> SendSetGNSS(GpsGlonass gpsGlonass, BeidouGalileo beidouGalileo, byte Power)
        {
            var header = new MessageHeader(this._addressSender, this._addressReceiver, AmpCodes.NAVI_RADIAT_ON, CheckCounter(), GpsGlonass.BinarySize + BeidouGalileo.BinarySize + 1);
            var message = NavigationGpsGlonnasBeidouGalileoWihtPowerRequest.ToBinary(header, gpsGlonass, beidouGalileo, Power);
            DefaultMessage defaultMessage = await SendReceiveAsync<DefaultMessage>(message, DefaultMessage.BinarySize);
            return defaultMessage;
        }

        /// <summary>
        /// Установить подавляемые системы навигации и включить излучение (2 канала)
        /// </summary>
        public async Task<DefaultMessage> SendSetGNSSWhenSpoof(GpsGlonass gpsGlonass, BeidouGalileo beidouGalileo)
        {
            var header = new MessageHeader(this._addressSender, this._addressReceiver, AmpCodes.NAVI_RADIAT_ON_TWO_CHANNEL, CheckCounter(), GpsGlonass.BinarySize + BeidouGalileo.BinarySize);
            var message = NavigationGpsGlonnasBeidouGalileoRequest.ToBinary(header, gpsGlonass, beidouGalileo);
            DefaultMessage defaultMessage = await SendReceiveAsync<DefaultMessage>(message, DefaultMessage.BinarySize);
            return defaultMessage;
        }

        /// <summary>
        /// Установить подавляемые системы навигации и включить излучение (2 канала)
        /// </summary>
        public async Task<DefaultMessage> SendSetGNSSWhenSpoof(GpsGlonass gpsGlonass, BeidouGalileo beidouGalileo, byte Power)
        {
            var header = new MessageHeader(this._addressSender, this._addressReceiver, AmpCodes.NAVI_RADIAT_ON_TWO_CHANNEL, CheckCounter(), GpsGlonass.BinarySize + BeidouGalileo.BinarySize + 1);
            var message = NavigationGpsGlonnasBeidouGalileoWihtPowerRequest.ToBinary(header, gpsGlonass, beidouGalileo, Power);
            DefaultMessage defaultMessage = await SendReceiveAsync<DefaultMessage>(message, DefaultMessage.BinarySize);
            return defaultMessage;
        }

        /// <summary>
        /// Установить подавляемые системы навигации / спуфинг и включить излучение (Для 2-го скворечника Грозы-С)
        /// </summary>
        public async Task<DefaultMessage> SendSetGNSSWhenSpoof(GpsGlonass gpsGlonass, BeidouGalileo beidouGalileo, Antenna antenna, Spoofing spoofing)
        {
            var header = new MessageHeader(this._addressSender, this._addressReceiver, AmpCodes.NAVI_RADIAT_ON_ANTENNA, CheckCounter(), GpsGlonass.BinarySize + BeidouGalileo.BinarySize + 2);
            var message = NavigationGpsGlonnasBeidouGalileoSecondRequest.ToBinary(header, gpsGlonass, beidouGalileo, antenna, spoofing);
            DefaultMessage defaultMessage = await SendReceiveAsync<DefaultMessage>(message, DefaultMessage.BinarySize);
            return defaultMessage;
        }

        /// <summary>
        /// Установить подавляемые системы навигации / спуфинг и включить излучение c мощностью(Для 2-го скворечника Грозы-С)
        /// </summary>
        public async Task<DefaultMessage> SendSetGNSSWhenSpoof(GpsGlonass gpsGlonass, BeidouGalileo beidouGalileo, Antenna antenna, Spoofing spoofing, byte Power)
        {
            var header = new MessageHeader(this._addressSender, this._addressReceiver, AmpCodes.NAVI_RADIAT_ON_ANTENNA, CheckCounter(), GpsGlonass.BinarySize + BeidouGalileo.BinarySize + 3);
            var message = NavigationGpsGlonnasBeidouGalileoSecondWihtPowerRequest.ToBinary(header, gpsGlonass, beidouGalileo, antenna, spoofing, Power);
            DefaultMessage defaultMessage = await SendReceiveAsync<DefaultMessage>(message, DefaultMessage.BinarySize);
            return defaultMessage;
        }

        /// <summary>
        /// Включить спуфинг
        /// </summary>
        public async Task<DefaultMessage> SendSetSPOOF()
        {
            var header = new MessageHeader(this._addressSender, this._addressReceiver, AmpCodes.SPOOF_APP, CheckCounter(), 0);
            var message = EnableSpoofing.ToBinary(header);
            DefaultMessage defaultMessage = await SendReceiveAsync<DefaultMessage>(message, DefaultMessage.BinarySize);
            return defaultMessage;
        }

        /// <summary>
        /// Включить спуфинг с мощностью
        /// </summary>
        public async Task<DefaultMessage> SendSetSPOOF(byte Power)
        {
            var header = new MessageHeader(this._addressSender, this._addressReceiver, AmpCodes.SPOOF_APP, CheckCounter(), 1);
            var message = EnableSpoofingWithPower.ToBinary(header, Power);
            DefaultMessage defaultMessage = await SendReceiveAsync<DefaultMessage>(message, DefaultMessage.BinarySize);
            return defaultMessage;
        }


        /// <summary>
        /// Настройки преселектора
        /// </summary>
        public async Task<FullStatusModel> SendParamPreselector(bool[] amplifires)
        {
            BitArray myBitArray = new BitArray(amplifires);
            byte[] data = new byte[2];
            myBitArray.CopyTo(data, 0);

            var header = new MessageHeader(this._addressSender, this._addressReceiver, AmpCodes.PARAM_PRESELECTOR, CheckCounter(), 2);
            var message = PreselectorSettings.ToBinary(header, data[0], data[1]);
            FullStatusResponse defaultMessage = await SendReceiveAsync<FullStatusResponse>(message, 39);
            return GetFullStatus(defaultMessage);
        }


        /// <summary>
        /// Установить параметры РП для ИРИ ФРЧ и системы навигации. Включить излучение заданной длительности
        /// </summary>
        public async Task<FullStatusModel> SendParamFwsGnss(int iDuration, GpsGlonass gnss, BeidouGalileo beidouGalileo, FWSParametersWithPower[] tParamFWS, byte powerAll)
        {
            for (int i = 0; i < tParamFWS.Length; i++)
            {
                tParamFWS[i].Deviation = CreateDev(tParamFWS[i].Modulation, tParamFWS[i].Deviation);
            }

            var header = new MessageHeader(this._addressSender, this._addressReceiver, AmpCodes.PARAM_FWS_GNSS, CheckCounter(), (byte)(tParamFWS.Length* FWSParametersWithPower.BinarySize + 5));
            
            byte[] navigationSystem = new byte[1];
            var myBitArray = new BitArray(8, false);
            myBitArray[0] = gnss.GpsL1;
            myBitArray[1] = gnss.GpsL2;
            myBitArray[2] = gnss.GlnssL1;
            myBitArray[3] = gnss.GlnssL2;
            myBitArray[4] = beidouGalileo.BeidouL1;
            myBitArray[5] = beidouGalileo.BeidouL2;
            myBitArray[6] = beidouGalileo.GalileoL1;
            myBitArray[7] = beidouGalileo.GalileoL2;
            myBitArray.CopyTo(navigationSystem, 0);

            var message = TurnOnRadiationIRIFRSNavigationWithPowerRequest.ToBinary(header, iDuration, navigationSystem[0], powerAll, tParamFWS);
            FullStatusResponse defaultMessage = await SendReceiveAsync<FullStatusResponse>(message, 39);
            return GetFullStatus(defaultMessage);
        }


        /// <summary>
        /// Выключить излучение и включить преселектор
        /// </summary>
        public async Task<FullStatusModel> SendPreselectorOn(byte letter)
        {
            var header = new MessageHeader(this._addressSender, this._addressReceiver, AmpCodes.RADIAT_OFF_PRESELECTOR_ON, CheckCounter(), 1);
            var message = DefaultMessage.ToBinary(header, letter);
            FullStatusResponse defaultMessage = await SendReceiveAsync<FullStatusResponse>(message, 39);
            return GetFullStatus(defaultMessage);
        }

        /// <summary>
        /// Переключение реле
        /// </summary>
        public async Task<DefaultMessage> SendRelaySwitching(byte letter)
        {
            var header = new MessageHeader(this._addressSender, this._addressReceiver, AmpCodes.RELAY_SWITCHING, CheckCounter(), 1);
            var message = DefaultMessage.ToBinary(header, letter);
            DefaultMessage defaultMessage = await SendReceiveAsync<DefaultMessage>(message, DefaultMessage.BinarySize);
            return defaultMessage;
        }

        /// <summary>
        /// Полный статус
        /// </summary>
        public async Task<FullStatusModel> SendFullStatus(byte letter)
        {
            var header = new MessageHeader(this._addressSender, this._addressReceiver, AmpCodes.FULL_STATUS, CheckCounter(), 1);
            var message = DefaultMessage.ToBinary(header, letter);

            FullStatusResponse defaultMessage = await SendReceiveAsync<FullStatusResponse>(message, 39);
            return GetFullStatus(defaultMessage);
        }
        #endregion

        #region CreateArrayForSend
        private FullStatusModel GetFullStatus(FullStatusResponse fullStatusResponse)
        {
            if (fullStatusResponse.ErrorCode != 0)
                return new FullStatusModel(1);
            var status = new FullStatusModel();
            status.CodeError = fullStatusResponse.ErrorCode;
            status.Relay = Convert.ToBoolean(fullStatusResponse.Rele);

            byte[] amp = new byte[2] { fullStatusResponse.TurningOnAmplifiersLowByte, fullStatusResponse.TurningOnAmplifiersHighByte };
            BitArray myBitArray = new BitArray(amp);
            bool[] amplifiers = new bool[10];
            for (int i = 0; i < amplifiers.Length; i++)
            {
                amplifiers[i] = myBitArray[i];
            }
            status.Amplifiers = amplifiers;

            var paramAmp = new LetterState[5];
            for (int i = 0; i < 5; i++)
            {
                paramAmp[i] = new LetterState()
                {
                    Synthesizer = fullStatusResponse.LetterStateParam[i].Synthesizer,
                    Radiation = fullStatusResponse.LetterStateParam[i].Radiation,
                    Current = Convert.ToByte(fullStatusResponse.LetterStateParam[i].Current),
                    AmplifierPower = fullStatusResponse.LetterStateParam[i].AmplifierPower,
                    Error = fullStatusResponse.LetterStateParam[i].Error
                };

                unchecked
                {
                    paramAmp[i].Temperature = fullStatusResponse.LetterStateParam[i].Temperature;
                }
            }

            return status;
        }
        #endregion

        #region CreateDev
        private byte CreateDev(byte Modulation, int Dev)
        {
            if (Modulation == 0 || Modulation == 1)
            {
                return 0;
            }
            else if (Modulation == 4)
            {
                if (Dev >= 1000 && Dev <= 127000)
                {
                    Dev /= 1000;
                    return DevConverter(Dev, true);
                }
                else if (Dev < 1000)
                {
                    Dev /= 10;
                    return DevConverter(Dev, false);
                }
                return 0;
            }
            else if (Modulation == 5)
            {
                if (Dev >= 1000 && Dev <= 255000)
                {
                    Dev /= 1000;
                    return (byte)Dev;
                }
                else if (Dev < 1000)
                {
                    Dev /= 10;
                    return (byte)Dev;
                }
                return 0;
            }
            return 0;
        }

        private byte DevConverter(int Dev, bool highBit)
        {
            string s = Convert.ToString(Dev, 2); //Convert to binary in a string

            int[] bits = s.PadLeft(8, '0') // Add 0's from left
                         .Select(c => int.Parse(c.ToString())) // convert each char to int
                         .ToArray(); // Convert IEnumerable from select to Array
            BitArray b = new BitArray(8, false);
            for (int i = 1; i < 8; i++)
            {
                b[i] = Convert.ToBoolean(bits[i]);
            }
            b[0] = highBit;
            return ConvertToByte(b);
        }

        private byte ConvertToByte(BitArray bits)
        {
            if (bits.Count != 8)
            {
                throw new ArgumentException("bits");
            }
            byte[] bytes = new byte[1];
            var reversed = new BitArray(bits.Cast<bool>().Reverse().ToArray());
            reversed.CopyTo(bytes, 0);
            return bytes[0];
        }

        private byte CheckCounter()
        {
            if (_counter == 255)
                _counter = 0;
            _counter++;
            return _counter;
        }

        private byte GetFPSAddrReceiverSender()
        {
            switch (_fpsAddressReceiver)
            {
                case 2:
                    return 0x12;
                case 3:
                    return 0x13;
                case 4:
                    return 0x14;
                case 8:
                    return 0x17;
                case 9:
                    return 0x18;
                case 10:
                    return 0x19;
                default:
                    return 0x12;
            }
        }

        private byte CalculateCRC(byte[] data, int startPosition)
        {
            uint CRC = new uint();
            for (int i = startPosition; i < data.Length; i++)
            {
                CRC += data[i];
            }

            return Convert.ToByte(CRC % 255);
        }
        #endregion
    }
}
