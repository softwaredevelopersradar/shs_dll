﻿using llcss;
using SHS_DLL.Models;
using System;
using System.IO.Ports;
using System.Threading.Tasks;

namespace SHS_DLL
{
    public class BaseCom
    {
        #region Private
        private SerialPort _port;
        #endregion

        #region Events
        public event EventHandler<byte[]> OnReadByte;
        public event EventHandler<byte[]> OnWriteByte;
        public event EventHandler<bool> OnConnect;
        public event EventHandler<bool> OnDisconnect;
        #endregion

        #region Open, Close
        /// <summary>
        /// Открыть ComPort
        /// </summary>
        public bool OpenPort(string portName, Int32 baudRate, Parity parity, Int32 dataBits, StopBits stopBits)
        {
            if (_port == null)
                _port = new SerialPort();

            if (_port.IsOpen)
                ClosePort();

            try
            {
                _port.PortName = portName;
                _port.BaudRate = baudRate;
                _port.Parity = parity;
                _port.DataBits = dataBits;
                _port.StopBits = stopBits;
                _port.ReadTimeout = 6000;
                _port.Open();

                OnConnect?.Invoke(this, true);
                return true;
            }
            catch
            {
                OnConnect?.Invoke(this, false);
                return false;
            }
        }

        /// <summary>
        /// Закрыть ComPort
        /// </summary>
        public bool ClosePort()
        {
            try
            {
                _port.DiscardInBuffer();
                _port.DiscardOutBuffer();

                _port.Close();

                OnDisconnect?.Invoke(this, true);
                return true;
            }
            catch
            {
                OnDisconnect?.Invoke(this, false);
                return false;
            }

        }
        #endregion

        #region SendReceive
        public async Task<T> SendReceiveAsync<T>(byte[] request, int responseSize) where T : IBinarySerializable, new()
        {
            var response = new byte[responseSize];
            try
            {
                _port.DiscardInBuffer();
                lock (_port)
                {
                    _port.Write(request, 0, request.Length);
                    OnWriteByte?.Invoke(this, request);
                }

                await Task.Run(() =>
                {
                    _port.Read(response, 0, responseSize);
                });

                OnReadByte?.Invoke(this, response);

                var result = new T();
                result.Decode(response, 0);
                return result;
            }
            catch
            {
                switch (typeof(T))
                {
                    case Type t when t == typeof(LetterStateResponse):
                        {
                            LetterStateResponse letterState = default;
                            letterState.ErrorCode = 1;
                            return (T)(object)letterState;
                        }
                    case Type t when t == typeof(LetterStateIncludingAntennaResponse):
                        {
                            LetterStateIncludingAntennaResponse letterStateIncludingAntenna = default;
                            letterStateIncludingAntenna.ErrorCode = 1;
                            return (T)(object)letterStateIncludingAntenna;
                        }
                    case Type t when t == typeof(DefaultMessage):
                        {
                            DefaultMessage defaultValue = default;
                            defaultValue.ErrorCode = 1;
                            return (T)(object)defaultValue;
                        }
                    case Type t when t == typeof(FullStatusResponse):
                        {
                            FullStatusResponse fullStatusResponse = default;
                            fullStatusResponse.ErrorCode = 1;
                            return (T)(object)fullStatusResponse;
                        }
                    case Type t when t == typeof(FPSDefaultMessageResponse):
                        {
                            FPSDefaultMessageResponse fPSDefaultMessage = default;
                            fPSDefaultMessage.ErrorCode = 1;
                            return (T)(object)fPSDefaultMessage;
                        }
                    case Type t when t == typeof(FPSDefaultMessageResponse):
                        {
                            FPSDefaultMessageResponse fPSDefaultMessage = default;
                            fPSDefaultMessage.ErrorCode = 1;
                            return (T)(object)fPSDefaultMessage;
                        }
                }
                return default(T);
            }
        }
        #endregion
    }
}
