﻿using SHS_DLL;
using System;
using System.Collections.Generic;
using System.Windows.Media.Imaging;

namespace WPF_SHS
{
    public enum Channel
    {
        ALL_CHANNEL = 0xFF,
        _100_500 = 0x01,
        _500_2500 = 0x02,
        _2500_6000 = 0x03,
        GNSS = 0x04
    }

    public enum RadiatDuration
    {
        _0 = 0x00,
        _05 = 0x01,
        _1 = 0x02,
        _15 = 0x03,
        _2 = 0x04,
        _25 = 0x05,
        _3 = 0x06,
        _35 = 0x07,
        _4 = 0x08,
        _45 = 0x09,
        _5 = 0x0A,
        _55 = 0x0B,
        _6 = 0x0C,
        _65 = 0x0D,
        _7 = 0x0E,
        _75 = 0x0F,
        _8 = 0x10
    }

    public class LetterEventArgs : EventArgs
    {
        public byte Letter { get; private set; }

        public bool IsSixLetter { get; private set; } = false;

        public LetterEventArgs(byte Letter)
        {
            this.Letter = Letter;
        }

        public LetterEventArgs(byte Letter, bool IsSixLetter)
        {
            this.Letter = Letter;
            this.IsSixLetter = IsSixLetter;
        }
    }

    public class SetSpoofEventArgs : EventArgs
    {
        public bool PowerBool { get; private set; }
        public byte PowerByte { get; private set; }

        public SetSpoofEventArgs(bool PowerBool, byte PowerByte)
        {
            this.PowerBool = PowerBool;
            this.PowerByte = PowerByte;
        }
    }



    public class SetNaViEventArgs : EventArgs
    {
        public bool GPS_L1 { get; private set; }
        public bool GPS_L2 { get; private set; }
        public bool Glonass_L1 { get; private set; }
        public bool Glonass_L2 { get; private set; }
        public bool PowerBool { get; private set; }
        public byte PowerByte { get; private set; }

        public SetNaViEventArgs(bool GPS_L1, bool GPS_L2, bool Glonass_L1, bool Glonass_L2, bool PowerBool, byte PowerByte)
        {
            this.GPS_L1 = GPS_L1;
            this.GPS_L2 = GPS_L2;
            this.Glonass_L1 = Glonass_L1;
            this.Glonass_L2 = Glonass_L2;
            this.PowerBool = PowerBool;
            this.PowerByte = PowerByte;
        }
    }

    public class SetGNSSEventArgs : EventArgs
    {
        public bool GPS_L1 { get; private set; }
        public bool GPS_L2 { get; private set; }
        public bool Glonass_L1 { get; private set; }
        public bool Glonass_L2 { get; private set; }
        public bool Beidou_L1 { get; private set; }
        public bool Beidou_L2 { get; private set; }
        public bool Galileo_L1 { get; private set; }
        public bool Galileo_L2 { get; private set; }
        public bool PowerBool { get; private set; }
        public byte PowerByte { get; private set; }

        public SetGNSSEventArgs(bool GPS_L1, bool GPS_L2, bool Glonass_L1, bool Glonass_L2, bool Beidou_L1, bool Beidou_L2, bool Galileo_L1, bool Galileo_L2, bool PowerBool, byte PowerByte)
        {
            this.GPS_L1 = GPS_L1;
            this.GPS_L2 = GPS_L2;
            this.Glonass_L1 = Glonass_L1;
            this.Glonass_L2 = Glonass_L2;
            this.Beidou_L1 = Beidou_L1;
            this.Beidou_L2 = Beidou_L2;
            this.Galileo_L1 = Galileo_L1;
            this.Galileo_L2 = Galileo_L2;
            this.PowerBool = PowerBool;
            this.PowerByte = PowerByte;
        }
    }

    public class SetGNSSWithAntennaEventArgs : EventArgs
    {
        public bool GPS_L1 { get; private set; }
        public bool GPS_L2 { get; private set; }
        public bool Glonass_L1 { get; private set; }
        public bool Glonass_L2 { get; private set; }
        public bool Beidou_L1 { get; private set; }
        public bool Beidou_L2 { get; private set; }
        public bool Galileo_L1 { get; private set; }
        public bool Galileo_L2 { get; private set; }
        public Antenna Antenna { get; private set; }
        public Spoofing Spoofing { get; private set; }
        public bool PowerBool { get; private set; }
        public byte PowerByte { get; private set; }

        public SetGNSSWithAntennaEventArgs(bool GPS_L1, bool GPS_L2, bool Glonass_L1, bool Glonass_L2, bool Beidou_L1, bool Beidou_L2, bool Galileo_L1, bool Galileo_L2, Antenna Antenna, Spoofing Spoofing, bool PowerBool, byte PowerByte)
        {
            this.GPS_L1 = GPS_L1;
            this.GPS_L2 = GPS_L2;
            this.Glonass_L1 = Glonass_L1;
            this.Glonass_L2 = Glonass_L2;
            this.Beidou_L1 = Beidou_L1;
            this.Beidou_L2 = Beidou_L2;
            this.Galileo_L1 = Galileo_L1;
            this.Galileo_L2 = Galileo_L2;
            this.PowerBool = PowerBool;
            this.PowerByte = PowerByte;
            this.Antenna = Antenna;
            this.Spoofing = Spoofing;
        }
    }

    public class SetPresEventArgs : EventArgs
    {
        public bool[] Pres { get; private set; }

        public SetPresEventArgs(bool[] Pres)
        {
            this.Pres = Pres;
        }
    }

    public class ParamEventArgs : EventArgs
    {
        public List<TableFreq> Tables { get; private set; }
        public bool PowerBool { get; private set; }
        public byte[] PowerByte { get; private set; }

        public ParamEventArgs(List<TableFreq> Tables, bool PowerBool, byte[] PowerByte)
        {
            this.Tables = Tables;
            this.PowerBool = PowerBool;
            this.PowerByte = PowerByte;
        }

    }

    public class SwitchEventArgs : EventArgs
    {
        public byte[] SectorNumber { get; private set; }
        /// <summary>
        /// true - receive, false - transmit
        /// </summary>
        public bool ReceiveTransmit { get; private set; }

        public SwitchEventArgs(byte[] sectorNumber, bool receiveTransmit)
        {
            SectorNumber = sectorNumber;
            ReceiveTransmit = receiveTransmit;
        }
    }

    public class SetParamFWSGNSSEventArgs : EventArgs
    {
        public SetGNSSEventArgs NaVi { get; private set; }
        public ParamEventArgs Param { get; private set; }
        public byte[] Power { get; private set; }

        public SetParamFWSGNSSEventArgs(SetGNSSEventArgs NaVi, ParamEventArgs Param, byte[] Power)
        {
            this.NaVi = NaVi;
            this.Param = Param;
            this.Power = Power;
        }
    }

    public class ForSecondTable : EventArgs
    {
        public BitmapImage Signal { get; set; }
        public BitmapImage Emitting { get; set; }
        public BitmapImage Power { get; set; }
        public bool Error { get; set; }

        public ForSecondTable(BitmapImage Signal, BitmapImage Emitting, BitmapImage Power, bool Error)
        {
            this.Signal = Signal;
            this.Emitting = Emitting;
            this.Power = Power;
            this.Error = Error;
        }
    }

    public class ForCalibrationControl : EventArgs
    {
        public int Freq { get; set; }
        public bool UU { get; set; }
        public bool AmplS { get; set; }
        public bool AmplZ { get; set; }
        public bool Power { get; set; }

        public ForCalibrationControl(int Freq, bool UU, bool AmplS, bool AmplZ, bool Power)
        {
            this.Freq = Freq;
            this.UU = UU;
            this.AmplS = AmplS;
            this.AmplZ = AmplZ;
            this.Power = Power;
        }
    }
}
