﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace WPF_SHS
{
    [Serializable]
    public class FTable : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        #region Private
        private byte id;
        private byte register;
        private int fStart;
        private int fStop;
        private byte kStart;
        private byte kStop;
        private bool status;
        #endregion

        public byte Register
        {
            get => register;
            set
            {
                if (register == value) return;
                register = value;
                OnPropertyChanged();
            }
        }

        public byte Id
        {
            get => id;
            set
            {
                if (id == value) return;
                id = value;
                OnPropertyChanged();
            }
        }

        public int FStart
        {
            get => fStart;
            set
            {
                if (fStart == value) return;
                fStart = value;
                OnPropertyChanged();
            }
        }

        public int FStop
        {
            get => fStop;
            set
            {
                if (fStop == value) return;
                fStop = value;
                OnPropertyChanged();
            }
        }

        public byte KStart
        {
            get => kStart;
            set
            {
                if (kStart == value) return;
                kStart = value;
                OnPropertyChanged();
            }
        }

        public byte KStop
        {
            get => kStop;
            set
            {
                if (kStop == value) return;
                kStop = value;
                OnPropertyChanged();
            }
        }

        public bool Status
        {
            get => status;
            set
            {
                if (status == value) return;
                status = value;
                OnPropertyChanged();
            }
        }

    }
}
