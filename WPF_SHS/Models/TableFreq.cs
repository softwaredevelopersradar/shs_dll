﻿using SHS_DLL;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace WPF_SHS
{
    public class TableFreq : INotifyPropertyChanged
    {
        #region Enum
        public enum hid : byte
        {
            [Description("Без модуляции")]
            FirstParam = 0,
            [Description("КФМ")]
            SecondParam = 1,
            [Description("ЛЧМ")]
            ThirdParam = 4,
            [Description("ЛЧМ2")]
            FourthParam = 5
        }
        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        #region Private
        private int id;
        private int freqKHz;
        private int dfreqKHz;
        private hid hindrance;
        private string forDataGrid;
        private int dev;
        private int man;
        private byte power;
        private int lit;
        private Antenna antenna;
        #endregion

        public int Id
        {
            get => id;
            set
            {
                if (id == value) return;
                id = value;
                OnPropertyChanged();
            }
        }

        public int FreqKHz
        {
            get => freqKHz;
            set
            {
                if (freqKHz == value) return;
                freqKHz = value;
                OnPropertyChanged();
            }
        }
        public int DFreqKHz
        {
            get => dfreqKHz;
            set
            {
                if (dfreqKHz == value) return;
                dfreqKHz = value;
                OnPropertyChanged();
            }
        }
        public string ForDataGrid
        {
            get => forDataGrid;
            set
            {
                if (forDataGrid == value) return;
                forDataGrid = value;
                OnPropertyChanged();
            }
        }
        public hid Hindrance
        {
            get => hindrance;
            set
            {
                if (hindrance == value) return;
                hindrance = value;
                OnPropertyChanged();
            }
        }
        public int Dev
        {
            get => dev;
            set
            {
                if (dev == value) return;
                dev = value;
                OnPropertyChanged();
            }
        }
        public int Man
        {
            get => man;
            set
            {
                if (man == value) return;
                man = value;
                OnPropertyChanged();
            }
        }
        public byte Power
        {
            get => power;
            set
            {
                if (power == value) return;
                power = value;
                OnPropertyChanged();
            }
        }
        public int Lit
        {
            get => lit;
            set
            {
                if (lit == value) return;
                lit = value;
                OnPropertyChanged();
            }
        }

        public Antenna Antenna
        {
            get => antenna;
            set
            {
                if (antenna == value) return;
                antenna = value;
                OnPropertyChanged();
            }
        }

    }

}
