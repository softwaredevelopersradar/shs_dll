﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace WPF_SHS
{
    public class STableFreq : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        #region Private
        private bool signal;
        private bool emitting;
        private string degree;
        private string amperage;
        private string lit;
        private bool power;
        private bool error;
        private string litDescrip;
        #endregion

        public string Lite
        {
            get => lit;
            set
            {
                if (lit == value) return;
                lit = value;
                OnPropertyChanged();
            }
        }

        public bool Signal
        {
            get => signal;
            set
            {
                if (signal == value) return;
                signal = value;
                OnPropertyChanged();
            }
        }

        public bool Emitting
        {
            get => emitting;
            set
            {
                if (emitting == value) return;
                emitting = value;
                OnPropertyChanged();
            }
        }

        public string Degree
        {
            get => degree;
            set
            {
                if (degree == value) return;
                degree = value;
                OnPropertyChanged();
            }
        }

        public string Amperage
        {
            get => amperage;
            set
            {
                if (amperage == value) return;
                amperage = value;
                OnPropertyChanged();
            }
        }

        public bool Power
        {
            get => power;
            set
            {
                if (power == value) return;
                power = value;
                OnPropertyChanged();
            }
        }

        public bool Error
        {
            get => error;
            set
            {
                if (error == value) return;
                error = value;
                OnPropertyChanged();
            }
        }
        public string LitDescrip
        {
            get => litDescrip;
            set
            {
                if (litDescrip == value) return;
                litDescrip = value;
                OnPropertyChanged();
            }
        }

        //public bool Signal
        //{
        //    get => signal;
        //    set
        //    {
        //        if (signal == value) return;
        //        signal = value;
        //        OnPropertyChanged();
        //    }
        //}

        //public bool Emitting
        //{
        //    get => emitting;
        //    set
        //    {
        //        if (emitting == value) return;
        //        emitting = value;
        //        OnPropertyChanged();
        //    }
        //}

        //public bool Power
        //{
        //    get => power;
        //    set
        //    {
        //        if (power == value) return;
        //        power = value;
        //        OnPropertyChanged();
        //    }
        //}

        //public bool Error
        //{
        //    get => error;
        //    set
        //    {
        //        if (error == value) return;
        //        error = value;
        //        OnPropertyChanged();
        //    }
        //}
    }
}
