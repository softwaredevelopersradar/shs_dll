﻿using MoreLinq;
using SHS_DLL;
using SHS_DLL.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Runtime.Serialization;
using System.Windows;
using System.Windows.Media;

namespace WPF_SHS
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            MyWindowInitLocation();
            GrozaSButton.Background = Brushes.White;
            GrozaZ1Button.Background = Brushes.DarkGray;
            FormButton.Background = Brushes.DarkGray;
            GrozaZ1.Visibility = Visibility.Collapsed;
            GrozaF.Visibility = Visibility.Collapsed;
            GrozaS.Visibility = Visibility.Visible;
            IntitComPort();
            OnConfigEthernet?.Invoke(this, true);
            ReadConfig();

            sHS.OnWriteByte += SHS_OnWriteByte;
            sHS.OnReadByte += SHS_OnReadByte;
            sHS.OnDisconnect += SHS_OnDisconnect;

            GrozaS.OnStatus += GrozaS_OnStatus;
            GrozaS.OnStatusAntenna += GrozaS_OnStatusAntenna;
            GrozaS.OnStatusAntennaSecond += GrozaS_OnStatusAntennaSecond;
            GrozaS.OnSwitch += GrozaS_OnSwitch;
            GrozaS.OnSetNaVi += GrozaS_OnSetNaVi;
            GrozaS.OnSetGNSS += GrozaS_OnSetGNSS;
            GrozaS.OnParam += GrozaS_OnParam;
            GrozaS.OnSpoof += GrozaS_OnSpoof;
            GrozaS.OnOffRad += GrozaS_OnOffRad;
            GrozaS.OnReset += GrozaS_OnReset;
            GrozaS.OnRelaySwitching += GrozaS_OnRelaySwitching;
            GrozaS.OnTestGNSS += GrozaS_OnTestGNSS;
            GrozaS.OnSetGNSSWhenSpoof += GrozaS_OnSetGNSSWhenSpoof;
            GrozaS.OnParamWithAntenna += GrozaS_OnParamWithAntenna;
            GrozaS.OnGNSSWithAntenna += GrozaS_OnGNSSWithAntenna;

            GrozaF.OnSetParam += GrozaF_OnSetParam;
            GrozaF.OnGetParam += GrozaF_OnGetParam;
            GrozaF.OnSaveParam += GrozaF_OnSaveParam;
            GrozaF.OnStatus += GrozaF_OnStatus;
            GrozaF.OnRadOFF += GrozaF_OnRadOFF;
            GrozaF.OnInterParam += GrozaF_OnInterParam;
            GrozaF.OnGNSS += GrozaF_OnGNSS;
            GrozaF.OnSwitchPosition += GrozaF_OnSwitchPosition;
            GrozaF.OnNaVi += GrozaF_OnNaVi;
            GrozaF.OnVersion += GrozaF_OnVersion;

            GrozaZ1.OnFullStatus += GrozaZ1_OnFullStatus;
            GrozaZ1.OnOffRadAndSetPres += GrozaZ1_OnOffRadAndSetPres;
            GrozaZ1.OnSetPres += GrozaZ1_OnSetPres;
            GrozaZ1.OnSetParam += GrozaZ1_OnSetParam;
            GrozaZ1.OnSwitch += GrozaS_OnSwitch;

            Display.DisplayTime = false;
        }

        private async void GrozaS_OnTestGNSS(object sender, SetGNSSEventArgs e)
        {
            if (Display.ShowAllByte == false)
                Display.AddTextToLog("Установить подавляемые системы навигации и включить излучение. Установить тестовый сигнал для GNSS без модуляции.", Brushes.Red);

            var result = await sHS.SendSetGNSS(new GpsGlonass() { GpsL1 = e.GPS_L1, GpsL2 = e.GPS_L2, GlnssL1 = e.Glonass_L1, GlnssL2 = e.Glonass_L2 }, new BeidouGalileo() { BeidouL1 = e.Beidou_L1, BeidouL2 = e.Beidou_L2, GalileoL1 = e.Galileo_L1, GalileoL2 = e.Galileo_L2 }, e.PowerByte);
            SHS_OnConfirmSet(result, AmpCodes.NAVI_RADIAT_ON);

            if(result.ErrorCode == 0)
            {
                byte resultTestGnss = await sHS.FPSSendSetTestGNSS();
                if (Display.ShowAllByte == false)
                {
                    Display.AddTextToLog($"Команда: Установить тестовый сигнал для GNSS без модуляции", Brushes.Green);
                    Display.AddTextToLog($"Код ошибки: {resultTestGnss}", Brushes.Green);
                }
            }
            else
            {
                Display.AddTextToLog("Ошибка установки подавляемой системы навигации.", Brushes.Purple);
            }
        }

        private async void GrozaS_OnSetGNSS(object sender, SetGNSSEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Установить подавляемые системы навигации и включить излучение 2", Brushes.Red);
                if (e.PowerBool == false)
                {
                    var result = await sHS.SendSetGNSS(new GpsGlonass() { GpsL1 = e.GPS_L1, GpsL2 = e.GPS_L2, GlnssL1 = e.Glonass_L1, GlnssL2 = e.Glonass_L2 }, new BeidouGalileo() { BeidouL1 = e.Beidou_L1, BeidouL2 = e.Beidou_L2, GalileoL1 = e.Galileo_L1, GalileoL2 = e.Galileo_L2 });
                    SHS_OnConfirmSet(result, AmpCodes.NAVI_RADIAT_ON);
                }
                else
                {
                    var result = await sHS.SendSetGNSS(new GpsGlonass() { GpsL1 = e.GPS_L1, GpsL2 = e.GPS_L2, GlnssL1 = e.Glonass_L1, GlnssL2 = e.Glonass_L2 }, new BeidouGalileo() { BeidouL1 = e.Beidou_L1, BeidouL2 = e.Beidou_L2, GalileoL1 = e.Galileo_L1, GalileoL2 = e.Galileo_L2 }, e.PowerByte);
                    SHS_OnConfirmSet(result, AmpCodes.NAVI_RADIAT_ON);
                }
            }
            catch { }
        }

        private async void GrozaS_OnGNSSWithAntenna(object sender, SetGNSSWithAntennaEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Установить подавляемые системы навигации и включить излучение 2", Brushes.Red);
                if (e.PowerBool == false)
                {
                    var result = await sHS.SendSetGNSSWhenSpoof(new GpsGlonass() { GpsL1 = e.GPS_L1, GpsL2 = e.GPS_L2, GlnssL1 = e.Glonass_L1, GlnssL2 = e.Glonass_L2 }, new BeidouGalileo() { BeidouL1 = e.Beidou_L1, BeidouL2 = e.Beidou_L2, GalileoL1 = e.Galileo_L1, GalileoL2 = e.Galileo_L2 }, e.Antenna, e.Spoofing);
                    SHS_OnConfirmSet(result, AmpCodes.NAVI_RADIAT_ON_ANTENNA);
                }
                else
                {
                    var result = await sHS.SendSetGNSSWhenSpoof(new GpsGlonass() { GpsL1 = e.GPS_L1, GpsL2 = e.GPS_L2, GlnssL1 = e.Glonass_L1, GlnssL2 = e.Glonass_L2 }, new BeidouGalileo() { BeidouL1 = e.Beidou_L1, BeidouL2 = e.Beidou_L2, GalileoL1 = e.Galileo_L1, GalileoL2 = e.Galileo_L2 }, e.Antenna, e.Spoofing, e.PowerByte);
                    SHS_OnConfirmSet(result, AmpCodes.NAVI_RADIAT_ON_ANTENNA);
                }
            }
            catch { }
        }


        private async void GrozaS_OnSetGNSSWhenSpoof(object sender, SetGNSSEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Установить подавляемые системы навигации и включить излучение при включенном спуфинге", Brushes.Red);
                if (e.PowerBool == false)
                {
                    var result = await sHS.SendSetGNSSWhenSpoof(new GpsGlonass() { GpsL1 = e.GPS_L1, GpsL2 = e.GPS_L2, GlnssL1 = e.Glonass_L1, GlnssL2 = e.Glonass_L2 }, new BeidouGalileo() { BeidouL1 = e.Beidou_L1, BeidouL2 = e.Beidou_L2, GalileoL1 = e.Galileo_L1, GalileoL2 = e.Galileo_L2 });
                    SHS_OnConfirmSet(result, AmpCodes.NAVI_RADIAT_ON_TWO_CHANNEL);
                }
                else
                {
                    var result = await sHS.SendSetGNSSWhenSpoof(new GpsGlonass() { GpsL1 = e.GPS_L1, GpsL2 = e.GPS_L2, GlnssL1 = e.Glonass_L1, GlnssL2 = e.Glonass_L2 }, new BeidouGalileo() { BeidouL1 = e.Beidou_L1, BeidouL2 = e.Beidou_L2, GalileoL1 = e.Galileo_L1, GalileoL2 = e.Galileo_L2 }, e.PowerByte);
                    SHS_OnConfirmSet(result, AmpCodes.NAVI_RADIAT_ON_TWO_CHANNEL);
                }
            }
            catch { }
        }

        private async void Calibration_OnStartScan(object sender, ForCalibrationControl e)
        {
            var paramFWS = new FWSParameters[] { new FWSParameters() { Freq = e.Freq, Deviation = 0, Duration = 0, Manipulation = 0, Modulation = 0 } };

            if (!bCount)
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Cтарт сканирования", Brushes.Red);
                bCount = true;
            }

            if (Display.ShowAllByte == false)
                Display.AddTextToLog("Установить параметры РП для ИРИ ФРЧ и включить излучение заданной длительности", Brushes.Red);

            byte setIriResult = 1;
            if (e.UU)
            {
                sHS._fpsAddressReceiver = GrozaF._fpsAddressReceiver;
                if(e.UU)
                {
                    setIriResult = await sHS.FPSSendSetParamsIRI(paramFWS);
                }
                else
                {
                    setIriResult = await sHS.FPSSendSetParamsIRIShort(paramFWS);
                }
                if (Display.ShowAllByte == false)
                {
                    Display.AddTextToLog($"Команда: Установить параметры помехи ИРИ и включить излучение", Brushes.Green);
                    Display.AddTextToLog($"Код ошибки: {setIriResult}", Brushes.Green);
                }
                if (AnswerForEthernet)
                    StartScan?.Invoke(this, true);
            }
            else if (e.AmplS)
            {
                if (e.Power == false)
                {
                    var result = await sHS.SendSetParamFWS(Convert.ToInt32(TimeBox.Value), paramFWS);
                    SHS_OnConfirmSet(result, AmpCodes.PARAM_FWS_APP);
                }
                else
                {
                    var paramFWSPower = new FWSParametersWithPower[paramFWS.Length];
                    for (int i = 0; i < paramFWS.Length; i++)
                    {
                        paramFWSPower[i] = new FWSParametersWithPower() { Freq = paramFWS[i].Freq, Deviation = paramFWS[i].Deviation, Duration = paramFWS[i].Deviation, Manipulation = paramFWS[i].Manipulation, Modulation = paramFWS[i].Manipulation, PowerLevel = 100 };
                    }
                    var result = await sHS.SendSetParamFWS(Convert.ToInt32(TimeBox.Value), paramFWSPower);
                    SHS_OnConfirmSet(result, AmpCodes.PARAM_FWS_APP);
                }
            }
            else if (e.AmplZ)
            {
                var paramFWSPower = new FWSParametersWithPower[paramFWS.Length];
                for (int i = 0; i < paramFWS.Length; i++)
                {
                    paramFWSPower[i] = new FWSParametersWithPower() { Freq = paramFWS[i].Freq, Deviation = paramFWS[i].Deviation, Duration = paramFWS[i].Deviation, Manipulation = paramFWS[i].Manipulation, Modulation = paramFWS[i].Manipulation, PowerLevel = 100 };
                }
                var result = await sHS.SendParamFwsGnss(Convert.ToInt32(TimeBox.Value), new GpsGlonass(), new BeidouGalileo(), paramFWSPower, 100);
                SHS_OnConfirmFullStatus(result, AmpCodes.PARAM_FWS_GNSS);
            }
        }

        private async void Calibration_OnStopScan(object sender, bool e)
        {
            if (Display.ShowAllByte == false)
                Display.AddTextToLog("Выключить излучение", Brushes.Red);
            if (!e)
            {
                var result = await sHS.SendRadiatOff(0);
                SHS_OnConfirmSet(result, AmpCodes.RADIAT_OFF_PRESELECTOR_ON);
            }
            else 
            {
                var result = await sHS.SendPreselectorOn(0);
                SHS_OnConfirmFullStatus(result, AmpCodes.RADIAT_OFF_PRESELECTOR_ON);
            }
        }

        public static event EventHandler<bool> StartScan;
        public static bool bCount = false;

        private async void Calibration_OnFreq(object sender, ForCalibrationControl e)
        {
            var paramFWS = new FWSParameters[] { new FWSParameters() { Freq = e.Freq, Deviation = 0, Duration = 0, Manipulation = 0, Modulation = 0 } };

            if (Display.ShowAllByte == false)
                Display.AddTextToLog("Установить параметры РП для ИРИ ФРЧ и включить излучение заданной длительности", Brushes.Red);

            if (e.UU)
            {
                byte setIriResult = 1;
                sHS._fpsAddressReceiver = GrozaF._fpsAddressReceiver;
                if (e.UU)
                {
                    setIriResult = await sHS.FPSSendSetParamsIRI(paramFWS);
                }
                else
                {
                    setIriResult = await sHS.FPSSendSetParamsIRIShort(paramFWS);
                }
                if (Display.ShowAllByte == false)
                {
                    Display.AddTextToLog($"Команда: Установить параметры помехи ИРИ и включить излучение", Brushes.Green);
                    Display.AddTextToLog($"Код ошибки: {setIriResult}", Brushes.Green);
                }
                if (AnswerForEthernet)
                    StartScan?.Invoke(this, true);
            }
            else if (e.AmplS)
            {
                if (e.Power == false)
                {
                    var result = await sHS.SendSetParamFWS(Convert.ToInt32(TimeBox.Value), paramFWS);
                    SHS_OnConfirmSet(result, AmpCodes.PARAM_FWS_APP);
                }
                else
                {
                    var paramFWSPower = new FWSParametersWithPower[paramFWS.Length];
                    for (int i = 0; i < paramFWS.Length; i++)
                    {
                        paramFWSPower[i] = new FWSParametersWithPower() { Freq = paramFWS[i].Freq, Deviation = paramFWS[i].Deviation, Duration = paramFWS[i].Deviation, Manipulation = paramFWS[i].Manipulation, Modulation = paramFWS[i].Manipulation, PowerLevel = 100 };
                    }
                    var result = await sHS.SendSetParamFWS(Convert.ToInt32(TimeBox.Value), paramFWSPower);
                    SHS_OnConfirmSet(result, AmpCodes.PARAM_FWS_APP);
                }
            }
            else if (e.AmplZ)
            {
                var paramFWSPower = new FWSParametersWithPower[paramFWS.Length];
                for (int i = 0; i < paramFWS.Length; i++)
                {
                    paramFWSPower[i] = new FWSParametersWithPower() { Freq = paramFWS[i].Freq, Deviation = paramFWS[i].Deviation, Duration = paramFWS[i].Deviation, Manipulation = paramFWS[i].Manipulation, Modulation = paramFWS[i].Manipulation, PowerLevel = 100 };
                }
                var result = await sHS.SendParamFwsGnss(Convert.ToInt32(TimeBox.Value), new GpsGlonass(), new BeidouGalileo(), paramFWSPower, 100);
                SHS_OnConfirmFullStatus(result, AmpCodes.PARAM_FWS_GNSS);
            }
        }

        public static bool AnswerForEthernet = false;

        private void Calibration_OnAddTextToDisplay(object sender, string e)
        {
            Dispatcher.Invoke(() => { Display.AddTextToLog($"{e}", Brushes.Green); });
        }

        private void SHS_OnVersion(object sender, byte e)
        {
            if (Display.ShowAllByte == false)
            {
                Dispatcher.Invoke(() => { Display.AddTextToLog($"Команда: Запросить версию ПО", Brushes.Green); });
                Dispatcher.Invoke(() => { Display.AddTextToLog($"Версия: {e}", Brushes.Green); });
            }
        }

        private void SHS_OnConfirmSwitch(PreselectorSettings preselectorSettings, AmpCodes code)
        {
            if (Display.ShowAllByte == false)
            {
                Dispatcher.Invoke(() => { Display.AddTextToLog($"Команда: {code}", Brushes.Green); });
                Dispatcher.Invoke(() => { Display.AddTextToLog($"Номер сектора: {preselectorSettings.TurningOnAmplifiersLowByte}", Brushes.Green); });
                Dispatcher.Invoke(() => { Display.AddTextToLog($"Номер сектора: {preselectorSettings.TurningOnAmplifiersHighByte}", Brushes.Green); });
            }
        }

        private async void GrozaF_OnVersion(object sender, EventArgs e)
        {
            try
            {
                byte result = 1;
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Запросить версию ПО", Brushes.Red);
                sHS._fpsAddressReceiver = GrozaF._fpsAddressReceiver;
                if (GrozaF.UU)
                {
                    result = await sHS.FPSSendGetVersion();
                }
                else
                {
                    result = await sHS.FPSSendGetVersionShort();
                }
                if (Display.ShowAllByte == false)
                {
                    Display.AddTextToLog($"Команда: Запросить версию ПО", Brushes.Green);
                    Display.AddTextToLog($"Версия: {result}", Brushes.Green);
                }
            }
            catch { }
        }

        private async void GrozaF_OnNaVi(object sender, (byte, byte) e)
        {
            try
            {
                var result = 1;
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Установить параметры помехи для навигации и включить излучение", Brushes.Red);
                sHS._fpsAddressReceiver = GrozaF._fpsAddressReceiver;
                if(GrozaF.UU)
                {
                    result = await sHS.FPSSendSetParamNaViRadOn(e.Item1, e.Item2);
                }
                else
                {
                    result = await sHS.FPSSendSetParamNaViRadOnShort(e.Item1, e.Item2);
                }

                if (Display.ShowAllByte == false)
                {
                    Display.AddTextToLog($"Команда: Установить параметры помехи для навигации и включить излучение", Brushes.Green);
                    Display.AddTextToLog($"Код ошибки: {result}", Brushes.Green);
                }
            }
            catch { }
        }

        private async void GrozaF_OnSwitchPosition(object sender, byte[] e)
        {
            try
            {
                byte result = 1;
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Установить положение коммутаторов", Brushes.Red);
                sHS._fpsAddressReceiver = GrozaF._fpsAddressReceiver;
                if (GrozaF.UU)
                {
                    result = await sHS.FPSSendPositionSwitches(e[0], e[1]);
                }
                else
                {
                    result = await sHS.FPSSendPositionSwitchesShort(e[0], e[1]);
                }

                if (Display.ShowAllByte == false)
                {
                    Display.AddTextToLog($"Команда: Установить положение коммутаторов", Brushes.Green);
                    Display.AddTextToLog($"Код ошибки: {result}", Brushes.Green);
                }
            }
            catch { }
        }

        private async void GrozaF_OnGNSS(object sender, EventArgs e)
        {
            try
            {
                byte result = 1;
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Установить тестовый сигнал для GNSS без модуляции", Brushes.Red);
                sHS._fpsAddressReceiver = GrozaF._fpsAddressReceiver;
                if (GrozaF.UU)
                {
                    result = await sHS.FPSSendSetTestGNSS();
                }
                else
                {
                    result = await sHS.FPSSendSetTestGNSSShort();
                }
                if (Display.ShowAllByte == false)
                {
                    Display.AddTextToLog($"Команда: Установить тестовый сигнал для GNSS без модуляции", Brushes.Green);
                    Display.AddTextToLog($"Код ошибки: {result}", Brushes.Green);
                }
            }
            catch { }
        }

        private async void GrozaF_OnInterParam(object sender, byte time)
        {
            FWSParameters[] paramFWS;
            List<TableFreq> e = new List<TableFreq>();
            if (GrozaS.Visibility == Visibility.Collapsed)
            {
                e = GrozaS.GetTableS();
            }
            else if (GrozaZ1.Visibility == Visibility.Visible)
            {
                e = GrozaZ1.GetTableZ1();
            }

            paramFWS = new FWSParameters[e.Count];

            for (int i = 0; i < e.Count; i++)
            {
                if (e[i].Hindrance == TableFreq.hid.FirstParam)
                {
                    paramFWS[i] = new FWSParameters() { Freq = e[i].FreqKHz, Deviation = 0, Manipulation = 0, Modulation = 0, Duration = time };
                }
                else if (e[i].Hindrance == TableFreq.hid.SecondParam)
                {
                    paramFWS[i] = new FWSParameters() { Freq = e[i].FreqKHz, Deviation = 0, Manipulation = (byte)e[i].Man, Modulation = 1, Duration = time };
                }
                else if (e[i].Hindrance == TableFreq.hid.ThirdParam)
                {
                    paramFWS[i] = new FWSParameters() { Freq = e[i].FreqKHz, Deviation = (byte)e[i].Dev, Manipulation = (byte)e[i].Man, Modulation = 4, Duration = time };
                }
                else if (e[i].Hindrance == TableFreq.hid.FourthParam)
                {
                    paramFWS[i] = new FWSParameters() { Freq = e[i].FreqKHz, Deviation = (byte)e[i].Dev, Manipulation = (byte)e[i].Man, Modulation = 5, Duration = time };
                }
            }
            if (Display.ShowAllByte == false)
                Display.AddTextToLog("Установить параметры помехи ИРИ и включить излучение", Brushes.Red);
            sHS._fpsAddressReceiver = GrozaF._fpsAddressReceiver;
            byte result = 1;
            if (GrozaF.UU)
            {
                result = await sHS.FPSSendSetParamsIRI(paramFWS);
            }
            else
            {
                result = await sHS.FPSSendSetParamsIRIShort(paramFWS);
            }
            if (Display.ShowAllByte == false)
            {
                Display.AddTextToLog($"Команда: Установить параметры помехи ИРИ и включить излучение", Brushes.Green);
                Display.AddTextToLog($"Код ошибки: {result}", Brushes.Green);
            }
            if (AnswerForEthernet)
                StartScan?.Invoke(this, true);
        }

        private async void GrozaF_OnRadOFF(object sender, byte e)
        {
            try
            {
                byte result = 1;
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Выключить излучение", Brushes.Red);
                sHS._fpsAddressReceiver = GrozaF._fpsAddressReceiver;
                if (GrozaF.UU)
                {
                    result = await sHS.SendFPSRadiatOFF(e);
                }
                else
                {
                    result = await sHS.SendFPSRadiatOFFShort(e);
                }
                if (Display.ShowAllByte == false)
                {
                    Display.AddTextToLog($"Команда: Выключить излучение", Brushes.Green);
                    Display.AddTextToLog($"Код ошибки: {result}", Brushes.Green);
                }
            }
            catch { }
        }

        private async void GrozaF_OnStatus(object sender, EventArgs e)
        {
            try
            {
                byte result = 1;
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Статус формирователя", Brushes.Red);
                sHS._fpsAddressReceiver = GrozaF._fpsAddressReceiver;
                if (GrozaF.UU)
                {
                    result = await sHS.FPSSendDeviceStatus();
                }
                else
                {
                    result = await sHS.FPSSendDeviceStatusShort();
                }

                if (Display.ShowAllByte == false)
                {
                    Display.AddTextToLog($"Команда: Статус формирователя", Brushes.Green);
                    string b = Convert.ToString(result, 2);
                    b = b.PadLeft(8, '0');
                    Display.AddTextToLog($"ADF1 {b[7]}", Brushes.Green);
                    Display.AddTextToLog($"ADF2 {b[6]}", Brushes.Green);
                    Display.AddTextToLog($"ADF3 {b[5]}", Brushes.Green);
                    Display.AddTextToLog($"ADF4 {b[4]}", Brushes.Green);
                    if (GrozaF.OneOf)
                    {
                        Display.AddTextToLog($"Нет ВЧ1 L1 L2 {b[2]}", Brushes.Green);
                        Display.AddTextToLog($"Нет ВЧ2 {b[1]}", Brushes.Green);
                        Display.AddTextToLog($"Нет ВЧ3 100-500 {b[0]}", Brushes.Green);
                    }
                    else
                    {
                        Display.AddTextToLog($"Нет ВЧ1 {b[2]}", Brushes.Green);
                        Display.AddTextToLog($"Нет ВЧ2 {b[1]}", Brushes.Green);
                    }
                }
            }
            catch { }
        }

        public static bool activiti = true; //true - S false - Z1
        public static event EventHandler<FTable[]> GetParams;

        private void SHS_OnConfirmGetParam(AmplifierParameters[] e)
        {
            FTable[] fTables = new FTable[e.Length];
            Dispatcher.Invoke(() => {
                for (int i = 0; i < e.Length; i++)
                {
                    fTables[i] = new FTable() { Register = e[i].Id, FStart = e[i].Fn, FStop = e[i].Fv, KStart = e[i].Kn, KStop = e[i].Kv, Status = Convert.ToBoolean(e[i].Status) };
                }
                if (Display.ShowAllByte == false)
                {
                    Display.AddTextToLog($"Команда: Запросить параметр усилителя в канале", Brushes.Green);
                }
                GetParams?.Invoke(this, fTables);
            });
        }

        private async void GrozaF_OnSaveParam(object sender, EventArgs e)
        {
            try
            {
                var result = 1;
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Сохранить в параметры усилителя в канале", Brushes.Red);
                sHS._fpsAddressReceiver = GrozaF._fpsAddressReceiver;
                if (GrozaF.UU)
                {
                    result = await sHS.FPSSendSaveParam();
                }
                else
                {
                    result = await sHS.FPSSendSaveParamShort();
                }
                if (Display.ShowAllByte == false)
                {
                    Display.AddTextToLog($"Команда: Сохранить в параметры усилителя в канале", Brushes.Green);
                    Display.AddTextToLog($"Код ошибки: {result}", Brushes.Green);
                }
            }
            catch
            {

            }
        }

        private async void GrozaF_OnGetParam(object sender, byte[] e)
        {
            try
            {
                AmplifierParameters[] result = new AmplifierParameters[1];
                byte[] indexes = new byte[2];
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Запросить параметр усилителя в канале", Brushes.Red);
                sHS._fpsAddressReceiver = GrozaF._fpsAddressReceiver;
                Array.Sort(e);
                if (GrozaF.UU)
                {
                    result  = await sHS.FPSSendGetParam(e);
                }
                else
                {
                    result = await sHS.FPSSendGetParamShort(e);
                }
                SHS_OnConfirmGetParam(result);
            }
            catch
            {

            }
        }

        private async void GrozaF_OnSetParam(object sender, FTable[] e)
        {
            try
            {
                byte result = 1;
                sHS._fpsAddressReceiver = GrozaF._fpsAddressReceiver;
                var paramFPS = new FPSParameters[e.Length];
                for (int i = 0; i < e.Length; i++)
                {
                    paramFPS[i] = new FPSParameters() { Id = e[i].Register, FreqH = e[i].FStop, FreqL = e[i].FStart, KH = e[i].KStop, KL = e[i].KStart };
                }
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Установить параметр усилителя в канале", Brushes.Red);
                if (GrozaF.UU)
                {
                    result = await sHS.FPSSendParam(paramFPS);
                }
                else
                {
                    result = await sHS.FPSSendParamShort(paramFPS);
                }
                if (Display.ShowAllByte == false)
                {
                    Display.AddTextToLog($"Команда: Установить параметр усилителя в канале", Brushes.Green);
                    Display.AddTextToLog($"Код ошибки: {result}", Brushes.Green);
                }
            }
            catch
            {

            }
        }

        #region Config
        private string COM;
        private string Baudrate;

        private void ReadConfig()
        {
            try
            {
                int count = 0;
                using (StreamReader sr = new StreamReader($"{AppDomain.CurrentDomain.BaseDirectory}Config.xml"))
                {
                    COM = sr.ReadLine();
                    Baudrate = sr.ReadLine();
                }
                foreach (var i in ComBox.Items)
                {
                    if (i.Equals(COM))
                        ComBox.SelectedIndex = count;
                    count++;
                }
                count = 0;
                foreach (var i in RateBox.Items)
                {
                    if (i.Equals(Baudrate))
                        RateBox.SelectedIndex = count;
                    count++;
                }
            }
            catch { }
        }

        private void CreateConfig()
        {
            try
            {
                string path = string.Format($"{AppDomain.CurrentDomain.BaseDirectory}Config.xml");
                using (StreamWriter sw = new StreamWriter(path, false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(COM);
                    sw.WriteLine(Baudrate);
                }
            }
            catch { }
        }
        #endregion

        #region Read, Write
        private void SHS_OnReadByte(object sender, byte[] e)
        {
            try
            {
                Dispatcher.Invoke(() => { ControlConnection.ShowRead(); });
                string a = "";
                foreach (var i in e)
                {
                    a += i.ToString("X2");
                    a += " ";
                }
                Dispatcher.Invoke(() => { Display.AddTextToLog(a, Brushes.Green); });
            }
            catch
            {

            }
        }

        private void SHS_OnWriteByte(object sender, byte[] e)
        {
            try
            {
                ControlConnection.ShowWrite();
                string a = "";
                foreach (var i in e)
                {
                    a += i.ToString("X2");
                    a += " ";
                }
                Display.AddTextToLog(a, Brushes.Red);
            }
            catch { }
        }
        #endregion

        #region Private GrozaS
        private bool CommandSetS = false;
        private bool[] S = new bool[] { false, false, false };
        private bool CommandResetS = false;
        #endregion

        #region Private GrozaZ1
        private bool CommandSet = false;
        private bool[] Z = new bool[] { false, false, false, false, false };
        private bool CommandReset = false;
        #endregion

        #region CommandForCrozaZ1
        private async void GrozaZ1_OnSetParam(object sender, SetParamFWSGNSSEventArgs e)
        {
            try
            {
                var paramFWS = new FWSParametersWithPower[e.Param.Tables.Count];
                for (int i = 0; i < e.Param.Tables.Count; i++)
                {
                    if (e.Param.Tables[i].Hindrance == TableFreq.hid.FirstParam)
                    {
                        paramFWS[i] = new FWSParametersWithPower() { Freq = e.Param.Tables[i].FreqKHz, Deviation = 0, Manipulation = 0, Modulation = 0, PowerLevel = e.Param.Tables[i].Power };
                    }
                    else if (e.Param.Tables[i].Hindrance == TableFreq.hid.SecondParam)
                    {
                        paramFWS[i] = new FWSParametersWithPower() { Freq = e.Param.Tables[i].FreqKHz, Deviation = 0, Manipulation = (byte)e.Param.Tables[i].Man, Modulation = 1, PowerLevel = e.Param.Tables[i].Power };
                    }
                    else if (e.Param.Tables[i].Hindrance == TableFreq.hid.ThirdParam)
                    {
                        paramFWS[i] = new FWSParametersWithPower() { Freq = e.Param.Tables[i].FreqKHz, Deviation = (byte)e.Param.Tables[i].Dev, Manipulation = (byte)e.Param.Tables[i].Man, Modulation = 4, PowerLevel = e.Param.Tables[i].Power };
                    }
                    else if (e.Param.Tables[i].Hindrance == TableFreq.hid.FourthParam)
                    {
                        paramFWS[i] = new FWSParametersWithPower() { Freq = e.Param.Tables[i].FreqKHz, Deviation = (byte)e.Param.Tables[i].Dev, Manipulation = (byte)e.Param.Tables[i].Man, Modulation = 5, PowerLevel = e.Param.Tables[i].Power };
                    }
                }
                foreach (var i in paramFWS)
                {
                    if (i.Freq >= 350000 && i.Freq <= 500000)
                    {
                        Z[0] = true;
                    }
                    else if (i.Freq >= 800000 && i.Freq <= 1500000)
                    {
                        Z[1] = true;
                    }
                    else if (i.Freq >= 2000000 && i.Freq <= 2700000)
                    {
                        Z[2] = true;
                    }
                    else if (i.Freq >= 5600000 && i.Freq <= 5900000)
                    {
                        Z[3] = true;
                    }
                }
                Z[4] = true;
                CommandSet = true;
                var gnss = new GpsGlonass() { GpsL1 = e.NaVi.GPS_L1, GpsL2 = e.NaVi.GPS_L2, GlnssL1 = e.NaVi.Glonass_L1, GlnssL2 = e.NaVi.Glonass_L2 };
                var beidouGalileo = new BeidouGalileo() { BeidouL1 = e.NaVi.Beidou_L1, BeidouL2 = e.NaVi.Beidou_L2, GalileoL1 = e.NaVi.Galileo_L1, GalileoL2 = e.NaVi.Galileo_L2 };
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Установить параметры РП для ИРИ ФРЧ и системы навигации. Включить излучение заданной длительности", Brushes.Red);
                var result = await sHS.SendParamFwsGnss(Convert.ToInt32(TimeBox.Value), gnss, beidouGalileo, paramFWS, e.NaVi.PowerByte);
                SHS_OnConfirmFullStatus(result, AmpCodes.PARAM_FWS_GNSS);
            }
            catch { }
        }

        private async void GrozaZ1_OnSetPres(object sender, SetPresEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Настройка преселектора", Brushes.Red);
                var result = await sHS.SendParamPreselector(e.Pres);
                SHS_OnConfirmFullStatus(result, AmpCodes.PARAM_PRESELECTOR);
            }
            catch { }
        }

        private async void GrozaZ1_OnOffRadAndSetPres(object sender, LetterEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Выключить излучение и Включить преселектор", Brushes.Red);
                var result = await sHS.SendPreselectorOn(e.Letter);
                SHS_OnConfirmFullStatus(result, AmpCodes.RADIAT_OFF_PRESELECTOR_ON);
                CommandReset = true;
            }
            catch { }
        }
        #endregion

        #region Full Status
        public static event EventHandler<FullStatusModel> OnAddFullStatusToSecondTable;

        private void SHS_OnConfirmFullStatus(FullStatusModel e, AmpCodes code)
        {
            try
            {
                if (AnswerForEthernet && e.Amp == AmpCodes.FULL_STATUS)
                {
                    Dispatcher.Invoke(() => { StartScan?.Invoke(this, true); });
                    return;
                }
                  if (Display.ShowAllByte == false)
                    {
                        Dispatcher.Invoke(() => { Display.AddTextToLog($"Команда: {code}", Brushes.Green); });
                        Dispatcher.Invoke(() => { Display.AddTextToLog($"Код ошибки: {e.CodeError}", Brushes.Green); });
                    }
                if (e.ParamAmp != null && e.ParamAmp.Length > 0)
                {
                    string a = e.Relay ? "ON" : "OFF";
                    Dispatcher.Invoke(() => { RelayLabel.Text = a; });

                    if (CommandSet)
                    {
                        for (int i = 0; i < e.ParamAmp.Length; i++)
                        {
                            if (/*e.ParamAmp[i].Error == 0 && */Z[i])
                            {
                                GrozaZ1.ZLit[i] = true;
                            }
                        }
                        CommandSet = false;
                    }

                    if (CommandReset)
                    {
                        for (int i = 0; i < e.ParamAmp.Length; i++)
                        {
                            if (/*e.ParamAmp[i].Error == 0 &&*/Z[i])
                            {
                                GrozaZ1.ZLit[i] = false;
                                Z[i] = false;
                            }
                        }
                        CommandReset = false;
                    }


                    OnAddFullStatusToSecondTable?.Invoke(this, e);
                }
            }
            catch
            { }
        }

        private async void GrozaZ1_OnFullStatus(object sender, LetterEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Полный статус", Brushes.Red);
                var result = await sHS.SendFullStatus(e.Letter);
                SHS_OnConfirmFullStatus(result, AmpCodes.FULL_STATUS);
            }
            catch { }
        }
        #endregion

        #region CommandForGrozaS

        #region RelaySwitch
        private async void GrozaS_OnRelaySwitching(object sender, LetterEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Переключение реле", Brushes.Red);
                var result = await sHS.SendRelaySwitching(e.Letter);
                SHS_OnConfirmSet(result, AmpCodes.RELAY_SWITCHING);
            }
            catch { }
        }
        #endregion

        private async void GrozaS_OnParam(object sender, ParamEventArgs e)
        {
            try
            {
                var paramFWS = new FWSParameters[e.Tables.Count];
                for (int i = 0; i < e.Tables.Count; i++)
                {
                    if (e.Tables[i].Hindrance == TableFreq.hid.FirstParam)
                    {
                        paramFWS[i] = new FWSParameters() { Freq = e.Tables[i].FreqKHz, Deviation = 0, Manipulation = 0, Modulation = 0 };
                    }
                    else if (e.Tables[i].Hindrance == TableFreq.hid.SecondParam)
                    {
                        paramFWS[i] = new FWSParameters() { Freq = e.Tables[i].FreqKHz, Deviation = 0, Manipulation = (byte)e.Tables[i].Man, Modulation = 1 };
                    }
                    else if (e.Tables[i].Hindrance == TableFreq.hid.ThirdParam)
                    {
                        paramFWS[i] = new FWSParameters() { Freq = e.Tables[i].FreqKHz, Deviation = (byte)e.Tables[i].Dev, Manipulation = (byte)e.Tables[i].Man, Modulation = 4 };
                    }
                    else if (e.Tables[i].Hindrance == TableFreq.hid.FourthParam)
                    {
                        paramFWS[i] = new FWSParameters() { Freq = e.Tables[i].FreqKHz, Deviation = (byte)e.Tables[i].Dev, Manipulation = (byte)e.Tables[i].Man, Modulation = 5 };
                    }
                }

                foreach (var i in paramFWS)
                {
                    if (i.Freq >= 100000 && i.Freq < 500000)
                    {
                        S[0] = true;
                    }
                    else if (i.Freq >= 500000 && i.Freq < 2500000)
                    {
                        S[1] = true;
                    }
                    else if (i.Freq >= 2500000 && i.Freq < 6000000)
                    {
                        S[2] = true;
                    }
                }
                CommandSetS = true;

                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Установить параметры РП для ИРИ ФРЧ и включить излучение заданной длительности", Brushes.Red);
                if (e.PowerBool == false)
                {
                    var result = await sHS.SendSetParamFWS(Convert.ToInt32(TimeBox.Value), paramFWS);
                    SHS_OnConfirmSet(result, AmpCodes.PARAM_FWS_APP);
                }
                else
                {
                    var paramFWSPower = new FWSParametersWithPower[paramFWS.Length];
                    for (int i = 0; i < paramFWS.Length; i++)
                    {
                        paramFWSPower[i] = new FWSParametersWithPower() { Freq = paramFWS[i].Freq, Deviation = paramFWS[i].Deviation, Duration = paramFWS[i].Deviation, Manipulation = paramFWS[i].Manipulation, Modulation = paramFWS[i].Manipulation, PowerLevel = e.PowerByte[i] };
                    }
                    var result = await sHS.SendSetParamFWS(Convert.ToInt32(TimeBox.Value), paramFWSPower);
                    SHS_OnConfirmSet(result, AmpCodes.PARAM_FWS_APP);
                }
            }
            catch { }
        }

        private async void GrozaS_OnParamWithAntenna(object sender, ParamEventArgs e)
        {
            try
            {
                var paramFWS = new FWSParametersWithAntennaPower[e.Tables.Count];
                for (int i = 0; i < e.Tables.Count; i++)
                {
                    if (e.Tables[i].Hindrance == TableFreq.hid.FirstParam)
                    {
                        paramFWS[i] = new FWSParametersWithAntennaPower() { Freq = e.Tables[i].FreqKHz, Deviation = 0, Manipulation = 0, Modulation = 0, AntennaParam = e.Tables[i].Antenna, PowerLevel = e.Tables[i].Power  };
                    }
                    else if (e.Tables[i].Hindrance == TableFreq.hid.SecondParam)
                    {
                        paramFWS[i] = new FWSParametersWithAntennaPower() { Freq = e.Tables[i].FreqKHz, Deviation = 0, Manipulation = (byte)e.Tables[i].Man, Modulation = 1, AntennaParam = e.Tables[i].Antenna, PowerLevel = e.Tables[i].Power };
                    }
                    else if (e.Tables[i].Hindrance == TableFreq.hid.ThirdParam)
                    {
                        paramFWS[i] = new FWSParametersWithAntennaPower() { Freq = e.Tables[i].FreqKHz, Deviation = (byte)e.Tables[i].Dev, Manipulation = (byte)e.Tables[i].Man, Modulation = 4, AntennaParam = e.Tables[i].Antenna, PowerLevel = e.Tables[i].Power };
                    }
                    else if (e.Tables[i].Hindrance == TableFreq.hid.FourthParam)
                    {
                        paramFWS[i] = new FWSParametersWithAntennaPower() { Freq = e.Tables[i].FreqKHz, Deviation = (byte)e.Tables[i].Dev, Manipulation = (byte)e.Tables[i].Man, Modulation = 5, AntennaParam = e.Tables[i].Antenna, PowerLevel = e.Tables[i].Power };
                    }
                }

                foreach (var i in paramFWS)
                {
                    if (i.Freq >= 100000 && i.Freq < 500000)
                    {
                        S[0] = true;
                    }
                    else if (i.Freq >= 500000 && i.Freq < 2500000)
                    {
                        S[1] = true;
                    }
                    else if (i.Freq >= 2500000 && i.Freq < 6000000)
                    {
                        S[2] = true;
                    }
                }
                CommandSetS = true;

                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Установить параметры РП для ИРИ ФРЧ и включить излучение заданной длительности (c учетом антенны) Для 2-го скворечника Грозы-С", Brushes.Red);
                if (e.PowerBool == false)
                {
                    var result = await sHS.SendSetParamFWSWithAntenna(Convert.ToInt32(TimeBox.Value), paramFWS);
                    SHS_OnConfirmSet(result, AmpCodes.PARAM_FWS_APP_ANTENNA);
                }
                else
                {
                    var paramFWSPower = new FWSParametersWithAntennaPower[paramFWS.Length];
                    for (int i = 0; i < paramFWS.Length; i++)
                    {
                        paramFWSPower[i] = new FWSParametersWithAntennaPower() { Freq = paramFWS[i].Freq, Deviation = paramFWS[i].Deviation, Duration = paramFWS[i].Deviation, Manipulation = paramFWS[i].Manipulation, Modulation = paramFWS[i].Manipulation, AntennaParam = paramFWS[i].AntennaParam, PowerLevel = e.Tables[i].Power };
                    }
                    var result = await sHS.SendSetParamFWSWithAntenna(Convert.ToInt32(TimeBox.Value), paramFWSPower);
                    SHS_OnConfirmSet(result, AmpCodes.PARAM_FWS_APP_ANTENNA);
                }
            }
            catch { }
        }


        #region Reset
        private async void GrozaS_OnReset(object sender, LetterEventArgs e)
        {
            try
            {
                CommandResetS = true;
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Сброс", Brushes.Red);
                var result  = await sHS.SendReset(e.Letter);
                SHS_OnConfirmSet(result, AmpCodes.RESET_APP);
            }
            catch { }
        }
        #endregion

        #region Off radiat
        private async void GrozaS_OnOffRad(object sender, LetterEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Выключить излучение", Brushes.Red);
                var result = await sHS.SendRadiatOff(e.Letter);
                SHS_OnConfirmSet(result, AmpCodes.RADIAT_OFF_APP);
            }
            catch { }
        }
        #endregion

        #region Spoof
        private async void GrozaS_OnSpoof(object sender, SetSpoofEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Включить спуфинг", Brushes.Red);
                if (e.PowerBool == false)
                {
                    var result = await sHS.SendSetSPOOF();
                    SHS_OnConfirmSet(result, AmpCodes.SPOOF_APP);
                }
                else
                {
                    var result = await sHS.SendSetSPOOF(e.PowerByte);
                    SHS_OnConfirmSet(result, AmpCodes.SPOOF_APP);
                }
            }
            catch { }
        }
        #endregion

        #region SetGnss
        private void SHS_OnConfirmSet(DefaultMessage message, AmpCodes code)
        {
            try
            {
                if (Display.ShowAllByte == false)
                {
                    Dispatcher.Invoke(() => { Display.AddTextToLog($"Команда: {code}", Brushes.Green); });
                    Dispatcher.Invoke(() => { Display.AddTextToLog($"Код ошибки: {message.ErrorCode}", Brushes.Green); });
                }
                if (AnswerForEthernet && code == AmpCodes.PARAM_FWS_APP)
                    Dispatcher.Invoke(() => { StartScan?.Invoke(this, true); });
            }
            catch { }
        }

        private async void GrozaS_OnSetNaVi(object sender, SetNaViEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Установка подавляемых систем навигации и включение излучения", Brushes.Red);
                if (e.PowerBool == false)
                {
                    var result = await sHS.SendSetGNSS(new GpsGlonass() { GpsL1 = e.GPS_L1, GpsL2 = e.GPS_L2, GlnssL1 = e.Glonass_L1, GlnssL2 = e.Glonass_L2 });
                    SHS_OnConfirmSet(result, AmpCodes.GNSS_APP);
                }
                else
                {
                    var result = await sHS.SendSetGNSS(new GpsGlonass() { GpsL1 = e.GPS_L1, GpsL2 = e.GPS_L2, GlnssL1 = e.Glonass_L1, GlnssL2 = e.Glonass_L2 }, e.PowerByte);
                    SHS_OnConfirmSet(result, AmpCodes.GNSS_APP);
                }
            }
            catch { }
        }


        #endregion

        #region Status
        public static event EventHandler<LetterState[]> OnAddStatusToSecondTable;
        private void SHS_OnConfirmStatus(LetterState[] LetterState)
        {
            try
            {
                if (CommandSet)
                {
                    for (int i = 0; i < LetterState.Length; i++)
                    {
                        if (Z[i])
                        {
                            GrozaS.SLit[i] = true;
                        }
                    }
                    CommandSetS = false;
                }

                if (CommandReset)
                {
                    for (int i = 0; i < LetterState.Length; i++)
                    {
                        if (Z[i])
                        {
                            GrozaS.SLit[i] = false;
                            S[i] = false;
                        }
                    }
                    CommandResetS = false;
                }

                if (LetterState != null && LetterState.Length > 0)
                {
                    OnAddStatusToSecondTable?.Invoke(this, LetterState);
                }
            }
            catch { }
        }

        private void SHS_OnConfirmStatusAntenna(LetterStateIncludingAntenna[] letterState)
        {
            try
            {
                if (CommandSet)
                {
                    for (int i = 0; i < letterState.Length; i++)
                    {
                        if (Z[i])
                        {
                            GrozaS.SLit[i] = true;
                        }
                    }
                    CommandSetS = false;
                }

                if (CommandReset)
                {
                    for (int i = 0; i < letterState.Length; i++)
                    {
                        if (Z[i])
                        {
                            GrozaS.SLit[i] = false;
                            S[i] = false;
                        }
                    }
                    CommandResetS = false;
                }

                string Antenna = string.Empty;
                if (letterState != null && letterState.Length > 0)
                {
                    Dispatcher.Invoke(() =>
                    {
                        for (int i = 0; i < letterState.Length; i++)
                        {
                            Antenna = letterState[i].AntennaParam == 0 ? "Направленная" : "Ненаправленная";
                            Display.AddTextToLog($"Антенна литера {i + 1}: {Antenna}", Brushes.Green);
                        }

                    });

                    LetterState[] letters = new LetterState[letterState.Length];
                    for (int i = 0; i < letterState.Length; i++)
                    {
                        letters[i] = new LetterState(letterState[i].Synthesizer, letterState[i].Radiation, letterState[i].Temperature, letterState[i].Current, letterState[i].AmplifierPower, letterState[i].Error);
                    }

                    OnAddStatusToSecondTable?.Invoke(this, letters);
                }
            }
            catch { }
        }

        private async void GrozaS_OnStatus(object sender, LetterEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Статус", Brushes.Red);
                var result = await sHS.SendStatus(e.Letter, e.IsSixLetter);
                SHS_OnConfirmStatus(result);
            }
            catch { }
        }

        private async void GrozaS_OnStatusAntenna(object sender, LetterEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Статус", Brushes.Red);
                var result = await sHS.SendStatusAntennaState(e.Letter, e.IsSixLetter);
                SHS_OnConfirmStatusAntenna(result);
            }
            catch { }
        }

        private async void GrozaS_OnStatusAntennaSecond(object sender, LetterEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Статус второго скворечника", Brushes.Red);
                var result = await sHS.SendStatusAntennaStateSecond(e.Letter);
                SHS_OnConfirmStatusAntenna(result);
            }
            catch { }
        }

        private async void GrozaS_OnSwitch(object sender, SwitchEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog( e.ReceiveTransmit == true ? "Настройка приёмных коммутаторов" : "Настройка передающих коммутаторов", Brushes.Red);

                if (e.ReceiveTransmit)
                {
                    var result = await sHS.SendSetReceivingSwitches(e.SectorNumber);
                    SHS_OnConfirmSwitch(result, AmpCodes.SET_RECEIVING_SWITCHES);
                }
                else
                {
                    var result = await sHS.SendSetTransmissionSwitches(e.SectorNumber);
                    SHS_OnConfirmSwitch(result, AmpCodes.SET_TRANSMISSION_SWITCHES);
                }
            }
            catch { }
        }
        #endregion

        #endregion

        private void SHS_OnDisconnect(object sender, bool e)
        {
            try
            {
                if (e)
                {
                    connectFlag = false;
                    Dispatcher.Invoke(() => { ControlConnection.ShowDisconnect(); });
                }
            }
            catch { }
        }

        public ComSHS sHS = new ComSHS(0x04, 0x05);

        private void IntitComPort()
        {
            RelayLabel.Text = "Null";
            int[] k = new int[201];

            for (int i = 0; i < 201; i++)
            {
                k[i] = i;
            }

            //foreach(var a in k)
            //{
            //    TimeBox.Items.Add(a.ToString());
            //}
            //TimeBox.SelectedIndex = 0;

            TimeBox.Value = 0;
            TimeBox.Minimum = 0;
            TimeBox.Maximum = 200;

            foreach (string PortName in SerialPort.GetPortNames())
            {
                ComBox.Items.Add(PortName);
            }
            ComBox.SelectedIndex = 0;

            string[] listbaudrate = new string[15];
            listbaudrate[0] = "110";
            listbaudrate[1] = "300";
            listbaudrate[2] = "600";
            listbaudrate[3] = "1200";
            listbaudrate[4] = "2400";
            listbaudrate[5] = "4800";
            listbaudrate[6] = "9600";
            listbaudrate[7] = "14400";
            listbaudrate[8] = "19200";
            listbaudrate[9] = "38400";
            listbaudrate[10] = "56000";
            listbaudrate[11] = "57600";
            listbaudrate[12] = "115200";
            listbaudrate[13] = "128000";
            listbaudrate[14] = "256000";

            foreach (string baudRate in listbaudrate)
            {
                RateBox.Items.Add(baudRate);
            }
            RateBox.SelectedIndex = 6;
        }

        private void GrozaSButton_Click(object sender, RoutedEventArgs e)
        {
            GrozaSButton.Background = Brushes.White;
            GrozaZ1Button.Background = Brushes.DarkGray;
            FormButton.Background = Brushes.DarkGray;
            GrozaZ1.Visibility = Visibility.Collapsed;
            GrozaS.Visibility = Visibility.Visible;
            GrozaF.Visibility = Visibility.Collapsed;
            TimeBox.Visibility = Visibility.Visible;
            TimeBoxLabel.Visibility = Visibility.Visible;
        }

        private void GrozaZ1Button_Click(object sender, RoutedEventArgs e)
        {
            GrozaZ1Button.Background = Brushes.White;
            GrozaSButton.Background = Brushes.DarkGray;
            FormButton.Background = Brushes.DarkGray;
            GrozaS.Visibility = Visibility.Collapsed;
            GrozaZ1.Visibility = Visibility.Visible;
            GrozaF.Visibility = Visibility.Collapsed;
            TimeBox.Visibility = Visibility.Visible;
            TimeBoxLabel.Visibility = Visibility.Visible;
        }

        private bool connectFlag = true;

        private void ControlConnection_ButServerClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (connectFlag)
                {
                    connectFlag = sHS.OpenPort(ComBox.SelectedItem.ToString(), Convert.ToInt32(RateBox.SelectedItem), Parity.None, 8, StopBits.One);
                    if (connectFlag)
                    {
                        ControlConnection.ShowConnect();
                        ControlConnection.ShowRead();
                        ControlConnection.ShowWrite();
                        COM = ComBox.SelectedItem.ToString();
                        Baudrate = RateBox.SelectedItem.ToString();
                        CreateConfig();
                    }
                    connectFlag = false;
                }
                else
                {
                    connectFlag = sHS.ClosePort();
                    ControlConnection.ShowDisconnect();
                    connectFlag = true;
                }
            }
            catch { }
        }

        public static event EventHandler<bool> OnConfigEthernet;

        private void MyWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                string path = string.Format($"{AppDomain.CurrentDomain.BaseDirectory}LocationMain.txt");
                using (StreamWriter sw = new StreamWriter(path, false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(MyWindow.Left);
                    sw.WriteLine(MyWindow.Top);
                }
                OnConfigEthernet?.Invoke(this, false);
                System.Windows.Threading.Dispatcher.ExitAllFrames();
            }
            catch { }
        }

        private void MyWindowInitLocation()
        {
            try
            {
                using (StreamReader sr = new StreamReader($"{AppDomain.CurrentDomain.BaseDirectory}LocationMain.txt"))
                {
                    MyWindow.Left = Convert.ToDouble(sr.ReadLine());
                    MyWindow.Top = Convert.ToDouble(sr.ReadLine());
                }
            }
            catch { }
        }

        private void CalibrationButton_Click(object sender, RoutedEventArgs e)
        {
            GrozaZ1Button.Background = Brushes.DarkGray;
            GrozaSButton.Background = Brushes.DarkGray;
            GrozaS.Visibility = Visibility.Collapsed;
            GrozaZ1.Visibility = Visibility.Collapsed;
            GrozaF.Visibility = Visibility.Collapsed;
            TimeBox.Visibility = Visibility.Collapsed;
            TimeBoxLabel.Visibility = Visibility.Collapsed;
        }

        private void FormButton_Click(object sender, RoutedEventArgs e)
        {
            FormButton.Background = Brushes.White;
            GrozaZ1Button.Background = Brushes.DarkGray;
            GrozaSButton.Background = Brushes.DarkGray;
            GrozaS.Visibility = Visibility.Collapsed;
            GrozaZ1.Visibility = Visibility.Collapsed;
            GrozaF.Visibility = Visibility.Visible;
            TimeBox.Visibility = Visibility.Collapsed;
            TimeBoxLabel.Visibility = Visibility.Collapsed;
        }

    }
}
