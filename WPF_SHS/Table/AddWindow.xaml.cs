﻿using System;
using System.Windows;

namespace WPF_SHS
{
    /// <summary>
    /// Логика взаимодействия для AddWindow.xaml
    /// </summary>
    public partial class AddWindow : Window
    {
        public AddWindow()
        {
            InitializeComponent();
            Control.OnButton += Control_onButton;
            Control.OnAdd();
        }

        private void Control_onButton(object sender, ControlAddToTable.ModelControl e)
        {
            var a = (byte)e.InterferenceParam;
            TableFreq table = new TableFreq
            {
                Id = 1,
                FreqKHz = e.FKHz,
                DFreqKHz = e.DFKHz,
                Hindrance = (TableFreq.hid)a,
                Dev = e.Dev,
                Man = e.Man,
                Lit = 0,
                Power = e.Power
            };
            OnAdd?.Invoke(this, table);
        }

        public event EventHandler<TableFreq> OnAdd;
    }
}
