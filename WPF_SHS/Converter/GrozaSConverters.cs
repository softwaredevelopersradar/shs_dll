﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace WPF_SHS
{
    public class CheckGrozaSToImgSourceConverter : IValueConverter
    {
        public static bool[] flag = new bool[3];
        public static int i = 0;

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!flag[i])
            {
                if (value is bool)
                {
                    return (bool)value ? new BitmapImage(new Uri("/Resources/red.png", UriKind.RelativeOrAbsolute)) : new BitmapImage(new Uri("/Resources/gray.png", UriKind.RelativeOrAbsolute));
                }
            }
            else if (flag[i])
            {
                if (value is bool)
                {
                    return (bool)value ? new BitmapImage(new Uri("/Resources/green.png", UriKind.RelativeOrAbsolute)) : new BitmapImage(new Uri("/Resources/red.png", UriKind.RelativeOrAbsolute));
                }
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }

    public class CheckGrozaValueToImgSourceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool)
            {
                return (bool)value ? new BitmapImage(new Uri("/Resources/green.png", UriKind.RelativeOrAbsolute)) : new BitmapImage(new Uri("/Resources/red.png", UriKind.RelativeOrAbsolute));
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }

    public class CheckGrozaSValueToImgSourceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool)
            {
                CheckGrozaSToImgSourceConverter.i++;
                return (bool)value ? new BitmapImage(new Uri("/Resources/red.png", UriKind.RelativeOrAbsolute)) : new BitmapImage(new Uri("/Resources/green.png", UriKind.RelativeOrAbsolute));
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}
