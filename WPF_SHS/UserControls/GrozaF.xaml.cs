﻿using Microsoft.Win32;
using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Serialization;

namespace WPF_SHS
{
    /// <summary>
    /// Логика взаимодействия для GrozaF.xaml
    /// </summary>
    public partial class GrozaF : UserControl
    {
        public GrozaF()
        {
            InitializeComponent();
            FTable.IsReadOnly = true;
            AddrReceiv.Items.Add(2);
            AddrReceiv.Items.Add(3);
            AddrReceiv.Items.Add(4);
            AddrReceiv.Items.Add(8);
            AddrReceiv.Items.Add(9);
            AddrReceiv.Items.Add(10);
            AddrReceiv.SelectedIndex = 0;
            ChannelBox.Items.Add("Все каналы");
            ChannelBox.Items.Add("100-500");
            ChannelBox.Items.Add("500-2500");
            ChannelBox.Items.Add("2500-6000");
            ChannelBox.Items.Add("GNSS");
            ChannelBox.SelectedIndex = 0;
            for (double i = 0; i <= 8; i += 0.5)
            {
                TimeRadBox.Items.Add(i);
            }
            TimeRadBox.SelectedIndex = 0;

            NumericPower.Minimum = 0;
            NumericPower.Maximum = 100;
            NumericPower.Value = 0;
            NumericNumber.Minimum = 0;
            NumericNumber.Maximum = 255;
            NumericNumber.Value = 0;
            NumericPosition.Minimum = 0;
            NumericPosition.Maximum = 255;
            NumericPosition.Value = 0;


            MainWindow.GetParams += MainWindow_GetParams;
        }

        private void MainWindow_GetParams(object sender, FTable[] e)
        {
            GrozaFConverter.checkBool = true;

            for (int i = 0; i < e.Length; i++)
            {
                for (int j = 0; j < FTable.Items.Count; j++)
                {
                    FTable.SelectedIndex = j;
                    FTable item = FTable.SelectedItem as FTable;
                    if (e[i].Register == item.Register)
                    {
                        item.FStart = e[i].FStart;
                        item.FStop = e[i].FStop;
                        item.KStart = e[i].KStart;
                        item.KStop = e[i].KStop;
                        item.Status = !item.Status;
                        item.Status = !item.Status;
                        item.Status = e[i].Status;
                    }
                }
            }

            GrozaFConverter.checkBool = false;
        }

        private int myId = 1;
        AddWindowF windowAdd;
        public static byte _fpsAddressReceiver = 0x02;

        private void BAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                windowAdd = new AddWindowF();
                windowAdd.LocationChanged += WindowAdd_LocationChanged;
                windowAdd.OnAdd += Window_onAdd;
                MyWindowInitLocation(windowAdd);
                windowAdd.ShowDialog();
            }
            catch
            {
                windowAdd.Close();
            }
        }

        ChangeWindowF windowChange;
        public static event EventHandler<FTable> Change;

        private void BChange_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (FTable.SelectedIndex > -1)
                {
                    windowChange = new ChangeWindowF();
                    windowChange.onChange += WindowChange_onChange;
                    windowChange.LocationChanged += WindowChange_LocationChanged;
                    var a = (FTable)FTable.SelectedItem;
                    Change?.Invoke(this, (FTable)FTable.SelectedItem);
                    MyWindowInitLocation(windowChange);
                    windowChange.ShowDialog();
                }
            }
            catch
            {
                windowChange.Close();
            }
        }

        private void BDelete_Click(object sender, RoutedEventArgs e)
        {
            if (FTable.SelectedIndex > -1)
            {
                FTable.Items.RemoveAt(FTable.SelectedIndex);
                for (int i = 0; i < FTable.Items.Count; i++)
                {
                    FTable.SelectedIndex = i;
                    FTable item = FTable.SelectedItem as FTable;
                    item.Id = (byte)(i + 1);
                }
                myId--;
            }
        }

        private void BClear_Click(object sender, RoutedEventArgs e)
        {
            FTable.Items.Clear();
            myId = 1;
        }

        private void Window_onAdd(object sender, FTable e)
        {
            try
            {
                for (int i = 0; i < FTable.Items.Count; i++)
                {
                    FTable.SelectedItem = i;
                    var a = FTable.SelectedItem as FTable;
                    if (a.Register == e.Register)
                    {
                        MessageBox.Show("Такой Id уже существует");
                        return;
                    }
                }
                if (e.KStart < 0)
                {
                    MessageBox.Show("Кstart меньше минимального значения диапазона 0");
                    return;
                }
                if (e.KStop > 65)
                {
                    MessageBox.Show("Кstop больше максимального значения диапазона 65");
                    return;
                }
                if (e.FStart < 100)
                {
                    MessageBox.Show("Fstart меньше минимального значения диапазона 100");
                    return;
                }
                if (e.FStop > 6000)
                {
                    MessageBox.Show("Fstop больше максимального значения диапазона 6000");
                    return;
                }
                if (e.FStart > e.FStop)
                {
                    MessageBox.Show("Fstart больше Fstop");
                    return;
                }
                if (e.KStart > e.KStop)
                {
                    MessageBox.Show("Kstart больше Kstop");
                    return;
                }
                FTable.Items.Add(new FTable() { Register = e.Register, FStart = e.FStart, FStop = e.FStop, KStart = e.KStart, KStop = e.KStop, Id = (byte)myId, Status = e.Status });
                myId++;
                FTable.SelectedIndex = FTable.Items.Count - 1;
                windowAdd.Close();
            }
            catch
            {
                windowAdd.Close();
            }
        }

        private void WindowChange_onChange(object sender, FTable e)
        {
            try
            {
                int ind = 0;
                if (FTable.SelectedIndex > -1)
                {
                    ind = FTable.SelectedIndex;
                    var item = (FTable)FTable.SelectedItem;

                    FTable.Items.RemoveAt(ind);

                    FTable.Items.Insert(ind, new FTable() { Id = (byte)ind, FStart = e.FStart, FStop = e.FStop, KStart = e.KStart, KStop = e.KStop, Register = e.Register });

                    FTable.SelectedIndex = ind;
                    windowChange.Close();

                }
            }
            catch { }
        }

        #region Location
        private void WindowAdd_LocationChanged(object sender, EventArgs e)
        {
            try
            {
                string path = string.Format($"{AppDomain.CurrentDomain.BaseDirectory}LocationAddChange.xml");
                using (StreamWriter sw = new StreamWriter(path, false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(windowAdd.Left);
                    sw.WriteLine(windowAdd.Top);
                }
            }
            catch { }
        }

        private void WindowChange_LocationChanged(object sender, EventArgs e)
        {
            try
            {
                string path = string.Format($"{AppDomain.CurrentDomain.BaseDirectory}LocationAddChange.xml");
                using (StreamWriter sw = new StreamWriter(path, false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(windowChange.Left);
                    sw.WriteLine(windowChange.Top);
                }
            }
            catch { }
        }

        private void MyWindowInitLocation(Window e)
        {
            try
            {
                using (StreamReader sr = new StreamReader($"{AppDomain.CurrentDomain.BaseDirectory}LocationAddChange.xml"))
                {
                    e.Left = Convert.ToDouble(sr.ReadLine());
                    e.Top = Convert.ToDouble(sr.ReadLine());
                }
            }
            catch { }
        }
        #endregion

        #region STable

        XmlSerializer formatter = new XmlSerializer(typeof(FTable[]));

        private void ReadConfig()
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            try
            {
                FTable.Items.Clear();
                myId = 1;

                openFileDialog1.InitialDirectory = $"{ AppDomain.CurrentDomain.BaseDirectory}";
                openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*|Yaml file(*.yaml)|*.yaml|Xml file(*.xml)|*.xml";
                openFileDialog1.FilterIndex = 2;
                openFileDialog1.RestoreDirectory = true;

                if (openFileDialog1.ShowDialog() == true)
                {

                    string filename = openFileDialog1.FileName;

                    using (FileStream fs = new FileStream(filename, FileMode.OpenOrCreate))
                    {
                        FTable[] item = (FTable[])formatter.Deserialize(fs);

                        foreach (FTable p in item)
                        {
                            FTable.Items.Add(p);
                            myId++;
                        }
                    }
                }
            }
            catch
            {
            }
        }

        private void CreateConfig()
        {
            try
            {
                SaveFileDialog saveFile = new SaveFileDialog();
                saveFile.InitialDirectory = $"{ AppDomain.CurrentDomain.BaseDirectory}";
                saveFile.Filter = "Text file(*.txt)|*.txt|c# file (*.cs)|*.cs|Yaml file(*.yaml)|*.yaml|Xml file(*.xml)|*.xml";
                if (saveFile.ShowDialog() == true)
                {
                    using (FileStream fs = new FileStream(saveFile.FileName, FileMode.OpenOrCreate))
                    {
                        var a = new FTable[FTable.Items.Count];
                        var k = FTable.SelectedIndex;
                        for (int i = 0; i < a.Length; i++)
                        {
                            FTable.SelectedIndex = i;
                            a[i] = FTable.SelectedItem as FTable;
                        }
                        FTable.SelectedIndex = k;
                        formatter.Serialize(fs, a);
                    }
                }
            }
            catch { }
        }

        #endregion

        private void SaveTableButton_Click(object sender, RoutedEventArgs e)
        {
            CreateConfig();
        }

        private void LoadTableButton_Click(object sender, RoutedEventArgs e)
        {
            ReadConfig();
        }

        public event EventHandler<FTable[]> OnSetParam;
        public event EventHandler<byte[]> OnGetParam;
        public event EventHandler OnSaveParam;
        public event EventHandler OnStatus;
        public event EventHandler<byte> OnInterParam;
        public event EventHandler<byte> OnRadOFF;
        public event EventHandler OnGNSS;
        public event EventHandler<byte[]> OnSwitchPosition;
        public event EventHandler<(byte,byte)> OnNaVi;
        public event EventHandler OnVersion;

        public static bool UU = false;
        public static bool OneOf = true; // true - first, false - second

        private void SetParamButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddrReceiver();
                FTable[] fTables = new FTable[FTable.SelectedItems.Count];
                for (int i = 0; i < FTable.SelectedItems.Count; i++)
                {
                    fTables[i] = FTable.SelectedItems[i] as FTable;
                }
                if ((bool)UUButton.IsChecked)
                    UU = true;
                else
                    UU = false;
                OnSetParam?.Invoke(this, fTables);
            }
            catch (Exception ex)
            {

            }
        }

        private void GetParamButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddrReceiver();
                FTable[] fTables = new FTable[FTable.SelectedItems.Count];
                byte[] id = new byte[FTable.SelectedItems.Count];
                for (int i = 0; i < FTable.SelectedItems.Count; i++)
                {
                    fTables[i] = FTable.SelectedItems[i] as FTable;
                    id[i] = fTables[i].Register;
                }
                if ((bool)UUButton.IsChecked)
                    UU = true;
                else
                    UU = false;
                OnGetParam?.Invoke(this, id);
            }
            catch
            {

            }
        }

        private void SaveParamButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddrReceiver();
                if ((bool)UUButton.IsChecked)
                    UU = true;
                else
                    UU = false;
                OnSaveParam?.Invoke(this, null);
            }
            catch { }
        }

        private void AddrReceiver()
        {
            _fpsAddressReceiver = Convert.ToByte(AddrReceiv.Text);
        }

        private void NaViBox_Unchecked(object sender, RoutedEventArgs e)
        {
            GPS_L1.IsChecked = false;
            GPS_L2.IsChecked = false;
            Glonass_L1.IsChecked = false;
            Glonass_L2.IsChecked = false;
            Beidou_L1.IsChecked = false;
            Beidou_L2.IsChecked = false;
            Galileo_L1.IsChecked = false;
            Galileo_L2.IsChecked = false;
        }

        private void NaViBox_Checked(object sender, RoutedEventArgs e)
        {
            GPS_L1.IsChecked = true;
            GPS_L2.IsChecked = true;
            Glonass_L1.IsChecked = true;
            Glonass_L2.IsChecked = true;
            Beidou_L1.IsChecked = true;
            Beidou_L2.IsChecked = true;
            Galileo_L1.IsChecked = true;
            Galileo_L2.IsChecked = true;
        }

        private void StatusButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddrReceiver();
                if ((bool)UUButton.IsChecked)
                    UU = true;
                else
                    UU = false;
                OneOf = Convert.ToByte(AddrReceiv.Text) == 1 ? true : false;
                OnStatus?.Invoke(this, null);
            }
            catch { }
        }

        private void RadOFFButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddrReceiver();
                if ((bool)UUButton.IsChecked)
                    UU = true;
                else
                    UU = false;

                #region Channel
                byte k = 0xff;
                if (ChannelBox.SelectedIndex == 1)
                    k = 0x01;
                else if (ChannelBox.SelectedIndex == 2)
                    k = 0x02;
                else if (ChannelBox.SelectedIndex == 3)
                    k = 0x03;
                else if (ChannelBox.SelectedIndex == 4)
                    k = 0x04;
                #endregion

                OnRadOFF?.Invoke(this, k);
            }
            catch { }
        }

        private void InterParamButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddrReceiver();
                if ((bool)UUButton.IsChecked)
                    UU = true;
                else
                    UU = false;
                OnInterParam?.Invoke(this, Convert.ToByte(TimeRadBox.SelectedIndex));
            }
            catch { }
        }

        private void GNSSButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddrReceiver();
                if ((bool)UUButton.IsChecked)
                    UU = true;
                else
                    UU = false;
                OnGNSS?.Invoke(this, null);
            }
            catch { }
        }

        private void ComPositionButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddrReceiver();
                if ((bool)UUButton.IsChecked)
                    UU = true;
                else
                    UU = false;
                OnSwitchPosition?.Invoke(this, new byte[2] { Convert.ToByte(NumericNumber.Value), Convert.ToByte(NumericPosition.Value) });
            }
            catch { }
        }

        private void NaViButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddrReceiver();
                if ((bool)UUButton.IsChecked)
                    UU = true;
                else
                    UU = false;
                string bits = Convert.ToString(Convert.ToInt16((bool)Galileo_L2.IsChecked), 2) + Convert.ToString(Convert.ToInt16((bool)Galileo_L1.IsChecked), 2) + Convert.ToString(Convert.ToInt16((bool)Beidou_L2.IsChecked), 2) + Convert.ToString(Convert.ToInt16((bool)Beidou_L1.IsChecked), 2) + Convert.ToString(Convert.ToInt16((bool)Glonass_L2.IsChecked), 2) + Convert.ToString(Convert.ToInt16((bool)Glonass_L1.IsChecked), 2) + Convert.ToString(Convert.ToInt16((bool)GPS_L2.IsChecked), 2) + Convert.ToString(Convert.ToInt16((bool)GPS_L1.IsChecked), 2);
                OnNaVi?.Invoke(this, (Convert.ToByte(bits, 2), (byte)NumericPower.Value));
            }
            catch { }
        }

        private void VersionButton_Click(object sender, RoutedEventArgs e)
        {
            AddrReceiver();
            if ((bool)UUButton.IsChecked)
                UU = true;
            else
                UU = false;
            OnVersion?.Invoke(this, null);
        }
    }
}
