﻿using SHS_DLL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Serialization;

namespace WPF_SHS
{
    /// <summary>
    /// Логика взаимодействия для GrozaZ1.xaml
    /// </summary>
    public partial class GrozaZ1 : UserControl
    {
        public GrozaZ1()
        {
            InitializeComponent();
            Dump();
        }

        #region Dump
        private void Dump()
        {
            FreqTable.IsReadOnly = true;
            SFreqTable.IsReadOnly = true;
            AddEmptyRowsToSecondTable();
            MainWindow.OnAddFullStatusToSecondTable += MainWindow_OnAddFullStatusToSecondTable;

            for (int i = 0; i < 4; i++)
            {
                LitComboBox.Items.Add(i);
            }

            for (int i = 1; i < 8; i++)
            {
                SectorNumberBoxByteSix.Items.Add(i);
                SectorNumberBoxByteSeven.Items.Add(i);
            }

            SectorNumberBoxByteSeven.SelectedIndex = 0;
            SectorNumberBoxByteSix.SelectedIndex = 0;
            LitComboBox.SelectedIndex = 0;
            ReadConfig();
        }
        #endregion

        #region Variables
        AddWindow windowAdd;
        ChangeWindow windowChange;
        private int myId = 1;
        XmlSerializer formatter = new XmlSerializer(typeof(TableFreq[]));
        public bool[] ZLit = new bool[] { false, false, false, false, false };
        private string[] RangeLit = new string[] { "350-500", "800-1500", "2000-2700", "5600-5900", "GNSS" };
        #endregion

        #region Event`s
        public event EventHandler<LetterEventArgs> OnFullStatus;
        public event EventHandler<LetterEventArgs> OnOffRadAndSetPres;
        public event EventHandler<SetPresEventArgs> OnSetPres;
        public event EventHandler<SetParamFWSGNSSEventArgs> OnSetParam;
        public event EventHandler<SwitchEventArgs> OnSwitch;
        public static event EventHandler<TableFreq> Change;
        #endregion

        #region Button`s
        private void AddEmptyRowsToSecondTable()
        {
            for (int i = 0; i < 5; i++)
            {
                SFreqTable.Items.Add(new STableFreq() { Lite = (i + 1).ToString(), LitDescrip = RangeLit[i], Degree = string.Empty, Amperage = string.Empty, Emitting = false, Error = true, Power = false, Signal = false });
            }
        }

        private void MainWindow_OnAddFullStatusToSecondTable(object sender, FullStatusModel e)
        {
            try
            {
                int count = 1;
                Dispatcher.Invoke(() => { SFreqTable.Items.Clear(); });
                CheckGrozaZ1ToImgSourceConverter.i = 0;
                foreach (var i in e.ParamAmp)
                {
                    Dispatcher.Invoke(() => { SFreqTable.Items.Add(new STableFreq() { Lite = count.ToString(), LitDescrip = RangeLit[count - 1], Signal = Convert.ToBoolean(i.Synthesizer), Emitting = Convert.ToBoolean(i.Radiation), Degree = i.Temperature.ToString(), Amperage = (i.Current / 10).ToString(), Power = Convert.ToBoolean(i.AmplifierPower), Error = Convert.ToBoolean(i.Error) }); });
                    count++;
                }
            }
            catch
            {
            }
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            firstCheck.IsChecked = true;
            secondCheck.IsChecked = true;
            thirdCheck.IsChecked = true;
            fourthCheck.IsChecked = true;
            fifthCheck.IsChecked = true;
            sixthCheck.IsChecked = true;
            seventhCheck.IsChecked = true;
            eighthCheck.IsChecked = true;
            ninthCheck.IsChecked = true;
            tenthCheck.IsChecked = true;
        }

        private void Preselector_Unchecked(object sender, RoutedEventArgs e)
        {
            firstCheck.IsChecked = false;
            secondCheck.IsChecked = false;
            thirdCheck.IsChecked = false;
            fourthCheck.IsChecked = false;
            fifthCheck.IsChecked = false;
            sixthCheck.IsChecked = false;
            seventhCheck.IsChecked = false;
            eighthCheck.IsChecked = false;
            ninthCheck.IsChecked = false;
            tenthCheck.IsChecked = false;
        }

        private void NaViBox_Checked(object sender, RoutedEventArgs e)
        {
            GPS_L1.IsChecked = true;
            GPS_L2.IsChecked = true;
            Glonass_L1.IsChecked = true;
            Glonass_L2.IsChecked = true;
            Beidou_L1.IsChecked = true;
            Beidou_L2.IsChecked = true;
            Galileo_L1.IsChecked = true;
            Galileo_L2.IsChecked = true;
        }

        private void NaViBox_Unchecked(object sender, RoutedEventArgs e)
        {
            GPS_L1.IsChecked = false;
            GPS_L2.IsChecked = false;
            Glonass_L1.IsChecked = false;
            Glonass_L2.IsChecked = false;
            Beidou_L1.IsChecked = false;
            Beidou_L2.IsChecked = false;
            Galileo_L1.IsChecked = false;
            Galileo_L2.IsChecked = false;
        }

        private void FullStatusButton_Click(object sender, RoutedEventArgs e)
        {
            OnFullStatus?.Invoke(this, new LetterEventArgs(0));
        }

        private void SetNaViButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                byte[] power = new byte[FreqTable.Items.Count];
                int x = FreqTable.SelectedIndex;
                List<TableFreq> tables = new List<TableFreq>();
                for (int i = 0; i < FreqTable.Items.Count; i++)
                {
                    FreqTable.SelectedIndex = i;
                    TableFreq item = FreqTable.SelectedItem as TableFreq;
                    tables.Add(item);
                    power[i] = item.Power;
                }
                FreqTable.SelectedIndex = x;
                OnSetParam?.Invoke(this, new SetParamFWSGNSSEventArgs(new SetGNSSEventArgs((bool)GPS_L1.IsChecked, (bool)GPS_L2.IsChecked, (bool)Glonass_L1.IsChecked, (bool)Glonass_L2.IsChecked, (bool)Beidou_L1.IsChecked, (bool)Beidou_L2.IsChecked, (bool)Galileo_L1.IsChecked, (bool)Galileo_L2.IsChecked, true, (byte)NumericPower.Value), new ParamEventArgs(tables, false, new byte[0]), power));
            }
            catch
            {
                MessageBox.Show("Выберите значение!");
            }
        }

        private void OffRadAndOnPresButton_Click(object sender, RoutedEventArgs e)
        {
            OnOffRadAndSetPres?.Invoke(this, new LetterEventArgs(Convert.ToByte(LitComboBox.Text)));
        }

        private void SettingsPresButton_Click(object sender, RoutedEventArgs e)
        {
            bool[] presArray = new bool[]
            {
                (bool)firstCheck.IsChecked,
                (bool)secondCheck.IsChecked,
                (bool)thirdCheck.IsChecked,
                (bool)fourthCheck.IsChecked,
                (bool)fifthCheck.IsChecked,
                (bool)sixthCheck.IsChecked,
                (bool)seventhCheck.IsChecked,
                (bool)eighthCheck.IsChecked,
                (bool)ninthCheck.IsChecked,
                (bool)tenthCheck.IsChecked,
            };
            OnSetPres?.Invoke(this, new SetPresEventArgs(presArray));
        }

        private void SetReceivingSwitch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OnSwitch?.Invoke(this, new SwitchEventArgs(new byte[] { Convert.ToByte(SectorNumberBoxByteSix.Text), Convert.ToByte(SectorNumberBoxByteSeven.Text) }, true));
            }
            catch { }
        }

        private void SetTransmissionSwitch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OnSwitch?.Invoke(this, new SwitchEventArgs(new byte[] { Convert.ToByte(SectorNumberBoxByteSix.Text), Convert.ToByte(SectorNumberBoxByteSeven.Text) }, false));
            }
            catch { }
        }
        #endregion

        #region FreqTable
        public List<TableFreq> GetTableZ1()
        {
            int x = FreqTable.SelectedIndex;
            List<TableFreq> tables = new List<TableFreq>();
            for (int i = 0; i < FreqTable.Items.Count; i++)
            {
                FreqTable.SelectedIndex = i;
                TableFreq item = FreqTable.SelectedItem as TableFreq;
                tables.Add(item);
            }
            FreqTable.SelectedIndex = x;
            return tables;
        }

        private int CheckLit(int e)
        {
            int count = 0;
            for (int i = 0; i < FreqTable.Items.Count; i++)
            {
                FreqTable.SelectedIndex = i;
                TableFreq item = FreqTable.SelectedItem as TableFreq;
                if (e == item.Lit)
                    count++;
            }
            return count;
        }

        private void WindowChange_onChange(object sender, TableFreq e)
        {
            int ind = 0;
            if (FreqTable.SelectedIndex > -1)
            {
                int L = 0;
                string Descrip = string.Empty;
                ind = FreqTable.SelectedIndex;
                var item = (TableFreq)FreqTable.SelectedItem;
                if ((e.FreqKHz >= 350000 && e.FreqKHz <= 500000) || (e.FreqKHz >= 800000 && e.FreqKHz <= 1500000) || (e.FreqKHz >= 2000000 && e.FreqKHz <= 2700000) || (e.FreqKHz >= 5600000 && e.FreqKHz <= 5900000))
                {
                    FreqTable.Items.RemoveAt(ind);
                    if (e.FreqKHz >= 350000 && e.FreqKHz <= 500000)
                    {
                        L = 1;
                        Descrip = "350-500";
                    }
                    else if (e.FreqKHz >= 800000 && e.FreqKHz <= 1500000)
                    {
                        L = 2;
                        Descrip = "800-1500";
                    }
                    else if (e.FreqKHz >= 2000000 && e.FreqKHz <= 2700000)
                    {
                        L = 3;
                        Descrip = "2000-2700";
                    }
                    else if (e.FreqKHz >= 5600000 && e.FreqKHz <= 5900000)
                    {
                        L = 4;
                        Descrip = "5600-5900";
                    }
                    if (L != 0)
                    {
                        if (e.Hindrance == TableFreq.hid.FirstParam)
                        {
                            FreqTable.Items.Insert(ind, new TableFreq() { Id = ind + 1, FreqKHz = e.FreqKHz, DFreqKHz = e.DFreqKHz, Hindrance = e.Hindrance, Lit = L, Dev = e.Dev, Man = e.Man, ForDataGrid = "Без модуляции", Power = e.Power, Antenna = e.Antenna });
                        }
                        else if (e.Hindrance == TableFreq.hid.SecondParam)
                        {
                            FreqTable.Items.Insert(ind, new TableFreq() { Id = ind + 1, FreqKHz = e.FreqKHz, DFreqKHz = e.DFreqKHz, Hindrance = e.Hindrance, Lit = L, Dev = e.Dev, Man = e.Man, ForDataGrid = $"КФМ {e.Man} мкс", Power = e.Power, Antenna = e.Antenna });
                        }
                        else if (e.Hindrance == TableFreq.hid.ThirdParam)
                        {
                            FreqTable.Items.Insert(ind, new TableFreq() { Id = ind + 1, FreqKHz = e.FreqKHz, DFreqKHz = e.DFreqKHz, Hindrance = e.Hindrance, Lit = L, Dev = e.Dev, Man = e.Man, ForDataGrid = $"ЛЧМ {e.Dev} кГц {e.Man} кГц/c", Power = e.Power, Antenna = e.Antenna });
                        }
                        else if (e.Hindrance == TableFreq.hid.FourthParam)
                        {
                            FreqTable.Items.Insert(ind, new TableFreq() { Id = ind + 1, FreqKHz = e.FreqKHz, DFreqKHz = e.DFreqKHz, Hindrance = e.Hindrance, Lit = L, Dev = e.Dev, Man = e.Man, ForDataGrid = $"ЛЧМ2 {e.Dev} кГц {e.Man} кГц/c", Power = e.Power, Antenna = e.Antenna });
                        }
                        FreqTable.SelectedIndex = ind;
                        CreateConfig();
                        windowChange.Close();
                    }
                    else
                    {
                        FreqTable.Items.Insert(ind, item);
                        FreqTable.SelectedIndex = ind;
                        MessageBox.Show("Значение для литеры существует!");
                    }
                }
                else
                {
                    MessageBox.Show("Проверьте вводимые данные!");
                }
            }
        }

        private void BAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                windowAdd = new AddWindow();
                windowAdd.LocationChanged += WindowAdd_LocationChanged;
                windowAdd.OnAdd += Window_onAdd;
                MyWindowInitLocation(windowAdd);
                windowAdd.ShowDialog();
            }
            catch
            {
                windowAdd.Close();
            }
        }

        private void Window_onAdd(object sender, TableFreq e)
        {
            int L = 0;
            string Descrip = string.Empty;
            if ((e.FreqKHz >= 350000 && e.FreqKHz <= 500000) || (e.FreqKHz >= 800000 && e.FreqKHz <= 1500000) || (e.FreqKHz >= 2000000 && e.FreqKHz <= 2700000) || (e.FreqKHz >= 5600000 && e.FreqKHz <= 5900000))
            {

                if (e.FreqKHz >= 350000 && e.FreqKHz <= 500000)
                {
                    L = 1;
                    Descrip = "350-500";
                }
                else if (e.FreqKHz >= 800000 && e.FreqKHz <= 1500000)
                {
                    L = 2;
                    Descrip = "800-1500";
                }
                else if (e.FreqKHz >= 2000000 && e.FreqKHz <= 2700000)
                {
                    L = 3;
                    Descrip = "2000-2700";
                }
                else if (e.FreqKHz >= 5600000 && e.FreqKHz <= 5900000)
                {
                    L = 4;
                    Descrip = "5600-5900";
                }
            }
            else
            {
                MessageBox.Show("Проверьте вводимые значения!");
                return;
            }

            if (e.Hindrance == TableFreq.hid.FirstParam)
            {
                FreqTable.Items.Add(new TableFreq() { Id = myId, FreqKHz = e.FreqKHz, DFreqKHz = e.DFreqKHz, Hindrance = e.Hindrance, Lit = L, Dev = 0, Man = 0, ForDataGrid = "Без модуляции", Power = e.Power, Antenna = e.Antenna });
            }
            else if (e.Hindrance == TableFreq.hid.SecondParam)
            {
                FreqTable.Items.Add(new TableFreq() { Id = myId, FreqKHz = e.FreqKHz, DFreqKHz = e.DFreqKHz, Hindrance = e.Hindrance, Lit = L, Dev = 0, Man = e.Man, ForDataGrid = $"КФМ {e.Man} мкс", Power = e.Power, Antenna = e.Antenna });
            }
            else if (e.Hindrance == TableFreq.hid.ThirdParam)
            {
                FreqTable.Items.Add(new TableFreq() { Id = myId, FreqKHz = e.FreqKHz, DFreqKHz = e.DFreqKHz, Hindrance = e.Hindrance, Lit = L, Dev = e.Dev, Man = e.Man, ForDataGrid = $"ЛЧМ {e.Dev} кГц {e.Man} кГц/c", Power = e.Power, Antenna = e.Antenna });
            }
            else if (e.Hindrance == TableFreq.hid.FourthParam)
            {
                FreqTable.Items.Add(new TableFreq() { Id = myId, FreqKHz = e.FreqKHz, DFreqKHz = e.DFreqKHz, Hindrance = e.Hindrance, Lit = L, Dev = e.Dev, Man = e.Man, ForDataGrid = $"ЛЧМ2 {e.Dev} кГц {e.Man} кГц/c", Power = e.Power, Antenna = e.Antenna });
            }
            myId++;
            CreateConfig();
            windowAdd.Close();
        }

        private void BDelete_Click(object sender, RoutedEventArgs e)
        {
            if (FreqTable.SelectedIndex > -1)
            {
                FreqTable.Items.RemoveAt(FreqTable.SelectedIndex);
                for (int i = 0; i < FreqTable.Items.Count; i++)
                {
                    FreqTable.SelectedIndex = i;
                    TableFreq item = FreqTable.SelectedItem as TableFreq;
                    item.Id = i + 1;
                }
                myId--;
            }
        }

        private void BClear_Click(object sender, RoutedEventArgs e)
        {
            FreqTable.Items.Clear();
            myId = 1;
        }

        private void BChange_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (FreqTable.SelectedIndex > -1)
                {
                    windowChange = new ChangeWindow();
                    windowChange.LocationChanged += WindowChange_LocationChanged;
                    windowChange.onChange += WindowChange_onChange;
                    var a = (TableFreq)FreqTable.SelectedItem;
                    Change?.Invoke(this, (TableFreq)FreqTable.SelectedItem);
                    MyWindowInitLocation(windowChange);
                    windowChange.ShowDialog();
                }
            }
            catch
            {
                windowChange.Close();
            }
        }

        #endregion

        #region Location
        private void WindowAdd_LocationChanged(object sender, EventArgs e)
        {
            try
            {
                string path = string.Format($"{AppDomain.CurrentDomain.BaseDirectory}LocationAddChange.xml");
                using (StreamWriter sw = new StreamWriter(path, false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(windowAdd.Left);
                    sw.WriteLine(windowAdd.Top);
                }
            }
            catch { }
        }

        private void WindowChange_LocationChanged(object sender, EventArgs e)
        {
            try
            {
                string path = string.Format($"{AppDomain.CurrentDomain.BaseDirectory}LocationAddChange.xml");
                using (StreamWriter sw = new StreamWriter(path, false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(windowChange.Left);
                    sw.WriteLine(windowChange.Top);
                }
            }
            catch { }
        }

        private void MyWindowInitLocation(Window e)
        {
            try
            {
                using (StreamReader sr = new StreamReader($"{AppDomain.CurrentDomain.BaseDirectory}LocationAddChange.xml"))
                {
                    e.Left = Convert.ToDouble(sr.ReadLine());
                    e.Top = Convert.ToDouble(sr.ReadLine());
                }
            }
            catch { }
        }
        #endregion

        #region ZTable

        private void ReadConfig()
        {
            try
            {
                using (FileStream fs = new FileStream($"{AppDomain.CurrentDomain.BaseDirectory}ZTable.xml", FileMode.OpenOrCreate))
                {
                    TableFreq[] item = (TableFreq[])formatter.Deserialize(fs);

                    foreach (TableFreq p in item)
                    {
                        FreqTable.Items.Add(p);
                        myId++;
                    }
                }
            }
            catch { }
        }

        private void CreateConfig()
        {
            try
            {
                using (FileStream fs = new FileStream($"{AppDomain.CurrentDomain.BaseDirectory}ZTable.xml", FileMode.Create))
                {
                    var a = new TableFreq[FreqTable.Items.Count];
                    var k = FreqTable.SelectedIndex;
                    for (int i = 0; i < a.Length; i++)
                    {
                        FreqTable.SelectedIndex = i;
                        a[i] = FreqTable.SelectedItem as TableFreq;
                    }
                    FreqTable.SelectedIndex = k;
                    formatter.Serialize(fs, a);
                }
            }
            catch { }
        }

        #endregion

    }
}
