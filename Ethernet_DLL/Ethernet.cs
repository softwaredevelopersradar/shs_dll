﻿using Ivi.Visa.Interop;
using System;
using System.Globalization;

namespace Ethernet_DLL
{
    public class Ethernet
    {
        //private ResourceManager rm = new ResourceManager();
        //private FormattedIO488 src = new FormattedIO488();
        public event EventHandler<string> OnMarkXY;
        public event EventHandler<string> OnFreq;
        public event EventHandler<string> OnMark;
        public event EventHandler<string> OnText;
        public event EventHandler<int> OnRepeatCommand;
        public event EventHandler<bool> OnEndScan;

        public enum ConnectionType
        {
            Ethernet = 0,
            USB
        }

        public string Connect(ConnectionType type, string VisaAddress)
        {
            try
            {
                //string AGILENT_N9X = string.Empty;
                //if (type == ConnectionType.Ethernet)
                //    AGILENT_N9X = VisaAddress;
                //else if (type == ConnectionType.USB)
                //    AGILENT_N9X = VisaAddress;

                //src.IO = (IMessage)rm.Open(AGILENT_N9X, AccessMode.NO_LOCK, 2000, "");

                //src.IO.WriteString("*IDN?");

                //string temp = src.IO.ReadString(48);

                //return temp;
                return "";
            }
            catch
            {
                return "Error";
            }
        }

        public bool Close()
        {
            try
            {
                //src.IO.Clear();
                //src.IO.Close();

                return true;
            }
            catch
            {
                return false;
            }
        }

        public string RefAmp(string refAmpValue)
        {
            try
            {
                //src.IO.WriteString(":DISP:WIND:TRAC:Y:RLEV " + refAmpValue + " dBm\n");

                //src.IO.WriteString(":DISP:WIND:TRAC:Y:RLEV?\n");
                //return src.IO.ReadString(18);
                return "";
            }
            catch
            {
                return "Error";
            }
        }

        public string RefOffset(string refOffsetValue, bool bEthernet)
        {
            try
            {
                //if(bEthernet)
                //{
                //    src.IO.WriteString(":DISPlay:WINDow:TRACe:Y:RLEVel:OFFSet " + refOffsetValue + "\n");
                //    src.IO.WriteString(":DISPlay:WINDow:TRACe:Y:RLEVel:OFFSet?\n");
                //    return src.IO.ReadString(18);
                //}
                return "It isn`t Ethernet port";
            }
            catch
            {
                return "Error";
            }
        }

        public string SetSpan(string spanFreqValue)
        {
            try
            {
                //src.IO.WriteString("SENS:FREQ:SPAN " + spanFreqValue + "000");
                //src.IO.WriteString("SENS:FREQ:SPAN?\n");
                //return src.IO.ReadString(18);
                return "";
            }
            catch
            {
                return "Error";
            }
        }

        public bool SysReset(bool bEthernet)
        {
            try
            {
                if (bEthernet)
                {
                    //src.IO.WriteString(":SYSTem:PRESet\n");
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        public bool SetPeak(bool bEthernet) // the same with Get DATA command
        {
            try
            {
                //string rData = string.Empty;
                if (bEthernet)
                {
                    //    rData = "0";
                    //    while (rData != "+1")
                    //    {
                    //        src.IO.Timeout = 2000;
                    //        src.IO.WriteString("*OPC?");
                    //        rData = src.IO.ReadString(2);
                    //    }

                    //    src.IO.WriteString("CALC:MARK:MAX\n");
                    //    src.IO.WriteString("CALC:MARK:X?\n");
                    //    rData = src.IO.ReadString(18);

                    //    src.IO.WriteString("CALC:MARK:Y?\n");

                    //    rData += ":" + src.IO.ReadString(19);

                    //    OnMarkXY?.Invoke(this, rData);
                }
                else
                {
                    //rData = "0";
                    //while (rData != "1\n")
                    //{
                    //    src.IO.Timeout = 2000;
                    //    src.IO.WriteString("*OPC?");
                    //    rData = src.IO.ReadString(2);
                    //}

                    //src.IO.WriteString("CALC:MARK:MAX\n");
                    //src.IO.WriteString("CALC:MARK:X?\n");
                    //rData = src.IO.ReadString(19);
                    //OnFreq?.Invoke(this, rData);

                    //src.IO.WriteString("CALC:MARK:Y?\n");
                    //rData = src.IO.ReadString(19);
                    //OnMark?.Invoke(this, rData);

                    //rData = "0";
                    //while (rData != "1\n")
                    //{
                    //    src.IO.Timeout = 2000;
                    //    src.IO.WriteString("*OPC?");
                    //    rData = src.IO.ReadString(2);
                    //}
                }

                //src.IO.WriteString("INIT:CONT ON\n");
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool SetKS(string centerFreqValue)
        {
            try
            {
                //src.IO.WriteString("SENS:FREQ:CENT " + centerFreqValue + "000");
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool startScan = false;

        public void SendFreq(int freq, int span, int stopFreq, bool avrg, int numberAvrg, bool bEthernet)
        {
            //try
            //{
            //    if (startScan)
            //    {
            //        string centerFreq = (freq * 1000).ToString();
            //        src.IO.WriteString("SENS:FREQ:CENT " + centerFreq);
            //        int count = 1;
            //        string txOPCData = "00";
            //        string YMARKData = "";
            //        string XMARKData = "";
            //        float datagetks = 0;

            //        if (avrg)
            //            count = numberAvrg;

            //        while (count > 0)
            //        {
            //            count--;

            //            src.IO.WriteString("INIT:CONT 0\n");
            //            src.IO.WriteString("INIT:IMM;*WAI\n");

            //            int CntrDataDelay = 0;
            //            if (bEthernet)
            //            {
            //                txOPCData = "00";
            //                while ((txOPCData != "+1") && (CntrDataDelay < 2000000))
            //                {
            //                    src.IO.Timeout = 2000;
            //                    CntrDataDelay++;
            //                    src.IO.WriteString("*OPC?");
            //                    txOPCData = src.IO.ReadString(2);
            //                }
            //            }
            //            else
            //            {
            //                txOPCData = "0\n";
            //                while ((txOPCData != "1\n") && (CntrDataDelay < 2000000))
            //                {
            //                    src.IO.Timeout = 2000;
            //                    CntrDataDelay++;
            //                    src.IO.WriteString("*OPC?");
            //                    txOPCData = src.IO.ReadString(2);
            //                }
            //            }

            //            if (CntrDataDelay > 2000000)
            //            {
            //                startScan = false;
            //                OnText?.Invoke(this, "KS WAIT ERROR!");
            //            }
            //            else
            //            {
            //                src.IO.WriteString("CALC:MARK:MAX\n");

            //                src.IO.WriteString("CALC:MARK:Y?\n");

            //                YMARKData = src.IO.ReadString(19);
            //                datagetks += float.Parse(YMARKData, CultureInfo.InvariantCulture.NumberFormat);

            //                CntrDataDelay = 0;

            //                if (bEthernet)
            //                {
            //                    txOPCData = "00";
            //                    while ((txOPCData != "+1") && (CntrDataDelay < 2000000))
            //                    {
            //                        src.IO.Timeout = 2000;
            //                        CntrDataDelay++;
            //                        src.IO.WriteString("*OPC?");
            //                        txOPCData = src.IO.ReadString(2);
            //                    }
            //                }
            //                else
            //                {
            //                    txOPCData = "0\n";
            //                    while ((txOPCData != "1\n") && (CntrDataDelay < 2000000))
            //                    {
            //                        src.IO.Timeout = 2000;
            //                        CntrDataDelay++;
            //                        src.IO.WriteString("*OPC?");
            //                        txOPCData = src.IO.ReadString(2);
            //                    }
            //                }
            //            }
            //            src.IO.WriteString("INIT:CONT ON\n");
            //        }

            //        if (avrg)
            //        {
            //            datagetks = datagetks / numberAvrg;
            //        }

            //        YMARKData = datagetks.ToString("0.0000");
            //        YMARKData = YMARKData.Replace(".", ",");
            //        XMARKData = freq.ToString();
            //        OnText?.Invoke(this, XMARKData + " : " + YMARKData);

            //        freq += span/1000;

            //        if (freq <= stopFreq)
            //        {

            //            if (freq == stopFreq)
            //            {
            //                freq -= 1;
            //            }
            //            OnRepeatCommand?.Invoke(this, freq * 1000);
            //        }
            //        else
            //        {
            //            startScan = false;

            //            OnText?.Invoke(this, "End Scan!");
            //        }
            //    }
            //}
            //catch
            //{
            //    startScan = false;
            //    OnEndScan?.Invoke(this, false);
            //    OnText?.Invoke(this, "CRC ERROR!");
            //}
        }
    }
}